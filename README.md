# 搭建教程

这是一个脚手架，方边快速开发。

## 1、基本环境

+ JDK1.8
+ MySQL5.7
+ IDEA2018以上
+ Maven3.5+

以上基本环境请自行百度安装

## 2、搭建工程架构

### 2.1 搭建Maven父工程

**1、创建工程**

![image-20210307152613133](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307152613133.png)



**2、选择Maven->JDK1.8然后直接next**

![image-20210307152651350](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307152651350.png)

**3、填写基本信息**

![image-20210307152912790](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307152912790.png)

**4、父工程搭建完毕**

![image-20210307153100013](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153100013.png)

### 2.2 创建QHSE主Module

**1、创建新Module**

![image-20210307153252652](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153252652.png)

**2、下一步**

![image-20210307153347361](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153347361.png)

**3、填写基本信息然后Finish**

![image-20210307153524277](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153524277.png)

**4、创建完成**

![image-20210307153637415](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153637415.png)

### 2.3 引入代码生成器

**1、下载renrne-generator代码生成器，网址：https://gitee.com/renrenio**

![image-20210307153851703](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153851703.png)



**2、点击进去选择下载**

![image-20210307153928256](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307153928256.png)



**3、解压**

![image-20210307154038819](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307154038819.png)

**4、将renren-generator移动wlhse目录下**

![image-20210307154238501](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307154238501.png)

![image-20210307154318708](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307154318708.png)

**6、添加到工程中**

![image-20210307154452877](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307154452877.png)

**7、添加Module标签并刷新**

![image-20210307154525074](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307154525074.png)

### 2.4 使用renren-generator

**1、目录结构**

![image-20210307155010020](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307155010020.png)

**2、观察application.yml**

学过springboot都知道，这个是配置文件，开源配置数据库连接信息，等各种信息。

![image-20210307155353995](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307155353995.png)

**3、观察generator.properties**

![image-20210307155922205](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307155922205.png)

**4、观察template**

![image-20210307160059783](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307160059783.png)

**5、总结**

```
application	是配置代码生成器要生成数据库的路径
generator   是配置包名和模块名用来动态跟新template
template    目录下的文件是模板，根据generator的配置就行替换
```

### 2.5 代码生成器生成代码

**1、修改数据库配置**

![image-20210307160628455](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307160628455.png)

**2、修改变量信息**

![image-20210307160836255](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307160836255.png)

**3、template都我们暂且不改**

**4、启动**

![image-20210307161013547](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307161013547.png)

**5、访问之前application.yml配置的：localhost:80**

![image-20210307161255008](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307161255008.png)

**6、生成代码下载**

![image-20210307161343315](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307161343315.png)

![image-20210307161443538](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307161443538.png)



**7、将解压之后的main目录的全部复制到qhse模块的main目录**

![image-20210307161916651](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307161916651.png)

**可以发现已经根据我们的配置生成了各个层次的代码，现在爆红是因为一些依赖没有导入**

### 2.6 创建wlhse-common模块

**为了方便统一管理包，我们添加一个通用模块存放通用的信息，并且统一导入包。**

**然后再在qhse模块中导入common模块即可**

**1、创建wlhse-common模块**

![image-20210307162512702](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307162512702.png)

**2、导入以下依赖**

```xml
<dependencies>
    <!--Mybatis-Plus-->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>3.2.0</version>
    </dependency>
    <!--MySQL驱动-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.22</version>
    </dependency>
    <!--lombok 依赖-->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.12</version>
    </dependency>
    <!--httpcore 依赖-->
    <dependency>
        <groupId>org.apache.httpcomponents</groupId>
        <artifactId>httpcore</artifactId>
        <version>4.4.12</version>
    </dependency>
    <!--commons-lang 依赖 -->
    <dependency>
        <groupId>commons-lang</groupId>
        <artifactId>commons-lang</artifactId>
        <version>2.6</version>
    </dependency>
    <!--  导入servlet-api 依赖  -->
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
        <scope>provided</scope>
    </dependency>
    <!--测试-->
    <dependency>
        <groupId>jakarta.validation</groupId>
        <artifactId>jakarta.validation-api</artifactId>
        <version>2.0.2</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```

3、在qhse模块引入wlhse-common

![image-20210307162826525](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307162826525.png)

4、下面是导入了common以及spring-web的pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>wlhse</artifactId>
        <groupId>com.wlhse</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>qhse</artifactId>

    <properties>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <spring-boot.version>2.3.7.RELEASE</spring-boot.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.wlhse</groupId>
            <artifactId>wlhse-common</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.3.7.RELEASE</version>
                <configuration>
                    <mainClass>web.demo.DemoApplication</mainClass>
                </configuration>
                <executions>
                    <execution>
                        <id>repackage</id>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

**5、添加工具**

**在导入了web、common以及shiro之后，我们发现仍然报错，这是因为renrne给我们封装了统一返回结果的工具类，我们需要导入。**

**为了减轻你们的压力，只需要复制我上传好的几个即可，这项目我会放到Gitee中**

![image-20210307164432437](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307164432437.png)

### 2.7 搭建完毕

**1、总体结构**

![image-20210307165502785](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307165502785.png)





**2、启动项目**

![image-20210307165635880](https://tobing-markdown.oss-cn-shenzhen.aliyuncs.com/image-20210307165635880.png)

## 3、总结

以上步骤是通过Maven父子工程整合了renren-generator、wlhse-common、qhse，其中：

renren-generator：可以根据数据库表生成各级代码，没得设计了新的数据库的时候，就要到此处选择新增的表生成代码，下载然后添加到项目中。

wlhse-common：生成的代码中依赖了wlhse-common的包，一般不用改这里的东西。

qhse：存放生成的代码，编写复杂逻辑的地方

## 4、技术栈

本项目采用的是SpringBoot + Mybatis-Plus，因此需要有Mybatis-Plus的基础，Mybatis-Plus的学习可以去B站找，或者直接去官方看看API即可。









