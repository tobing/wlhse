package com.wlhse.qhse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author tobing
 * @Date 2021/3/7 16:45
 * @Description
 */
@SpringBootApplication
public class QHSEApplcaiton {
    public static void main(String[] args) {
        SpringApplication.run(QHSEApplcaiton.class);
    }
}
