package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ChecklistEntity;
import com.wlhse.qhse.service.ChecklistService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@RestController
@RequestMapping("qhse/checklist")
public class ChecklistController {
    @Autowired
    private ChecklistService checklistService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:checklist:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = checklistService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{checklistid}")
    @RequiresPermissions("qhse:checklist:info")
    public R info(@PathVariable("checklistid") Integer checklistid){
		ChecklistEntity checklist = checklistService.getById(checklistid);

        return R.ok().put("checklist", checklist);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:checklist:save")
    public R save(@RequestBody ChecklistEntity checklist){
		checklistService.save(checklist);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:checklist:update")
    public R update(@RequestBody ChecklistEntity checklist){
		checklistService.updateById(checklist);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:checklist:delete")
    public R delete(@RequestBody Integer[] checklistids){
		checklistService.removeByIds(Arrays.asList(checklistids));

        return R.ok();
    }

}
