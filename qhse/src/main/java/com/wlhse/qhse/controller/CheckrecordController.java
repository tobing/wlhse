package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.CheckrecordEntity;
import com.wlhse.qhse.service.CheckrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:57
 */
@RestController
@RequestMapping("qhse/checkrecord")
public class CheckrecordController {
    @Autowired
    private CheckrecordService checkrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:checkrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = checkrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{checkrecordid}")
    @RequiresPermissions("qhse:checkrecord:info")
    public R info(@PathVariable("checkrecordid") Integer checkrecordid){
		CheckrecordEntity checkrecord = checkrecordService.getById(checkrecordid);

        return R.ok().put("checkrecord", checkrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:checkrecord:save")
    public R save(@RequestBody CheckrecordEntity checkrecord){
		checkrecordService.save(checkrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:checkrecord:update")
    public R update(@RequestBody CheckrecordEntity checkrecord){
		checkrecordService.updateById(checkrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:checkrecord:delete")
    public R delete(@RequestBody Integer[] checkrecordids){
		checkrecordService.removeByIds(Arrays.asList(checkrecordids));

        return R.ok();
    }

}
