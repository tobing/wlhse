package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DangerrecordEntity;
import com.wlhse.qhse.service.DangerrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/dangerrecord")
public class DangerrecordController {
    @Autowired
    private DangerrecordService dangerrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dangerrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dangerrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dangerrecord:info")
    public R info(@PathVariable("id") Integer id){
		DangerrecordEntity dangerrecord = dangerrecordService.getById(id);

        return R.ok().put("dangerrecord", dangerrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dangerrecord:save")
    public R save(@RequestBody DangerrecordEntity dangerrecord){
		dangerrecordService.save(dangerrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dangerrecord:update")
    public R update(@RequestBody DangerrecordEntity dangerrecord){
		dangerrecordService.updateById(dangerrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dangerrecord:delete")
    public R delete(@RequestBody Integer[] ids){
		dangerrecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
