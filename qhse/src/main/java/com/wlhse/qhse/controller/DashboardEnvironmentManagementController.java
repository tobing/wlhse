package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardEnvironmentManagementEntity;
import com.wlhse.qhse.service.DashboardEnvironmentManagementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@RestController
@RequestMapping("qhse/dashboardenvironmentmanagement")
public class DashboardEnvironmentManagementController {
    @Autowired
    private DashboardEnvironmentManagementService dashboardEnvironmentManagementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardenvironmentmanagement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardEnvironmentManagementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardenvironmentmanagement:info")
    public R info(@PathVariable("id") Integer id){
		DashboardEnvironmentManagementEntity dashboardEnvironmentManagement = dashboardEnvironmentManagementService.getById(id);

        return R.ok().put("dashboardEnvironmentManagement", dashboardEnvironmentManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardenvironmentmanagement:save")
    public R save(@RequestBody DashboardEnvironmentManagementEntity dashboardEnvironmentManagement){
		dashboardEnvironmentManagementService.save(dashboardEnvironmentManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardenvironmentmanagement:update")
    public R update(@RequestBody DashboardEnvironmentManagementEntity dashboardEnvironmentManagement){
		dashboardEnvironmentManagementService.updateById(dashboardEnvironmentManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardenvironmentmanagement:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardEnvironmentManagementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
