package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardQualityManagementEntity;
import com.wlhse.qhse.service.DashboardQualityManagementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@RestController
@RequestMapping("qhse/dashboardqualitymanagement")
public class DashboardQualityManagementController {
    @Autowired
    private DashboardQualityManagementService dashboardQualityManagementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardqualitymanagement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardQualityManagementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardqualitymanagement:info")
    public R info(@PathVariable("id") Integer id){
		DashboardQualityManagementEntity dashboardQualityManagement = dashboardQualityManagementService.getById(id);

        return R.ok().put("dashboardQualityManagement", dashboardQualityManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardqualitymanagement:save")
    public R save(@RequestBody DashboardQualityManagementEntity dashboardQualityManagement){
		dashboardQualityManagementService.save(dashboardQualityManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardqualitymanagement:update")
    public R update(@RequestBody DashboardQualityManagementEntity dashboardQualityManagement){
		dashboardQualityManagementService.updateById(dashboardQualityManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardqualitymanagement:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardQualityManagementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
