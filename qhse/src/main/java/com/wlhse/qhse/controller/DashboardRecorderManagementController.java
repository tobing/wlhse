package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardRecorderManagementEntity;
import com.wlhse.qhse.service.DashboardRecorderManagementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@RestController
@RequestMapping("qhse/dashboardrecordermanagement")
public class DashboardRecorderManagementController {
    @Autowired
    private DashboardRecorderManagementService dashboardRecorderManagementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardrecordermanagement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardRecorderManagementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardrecordermanagement:info")
    public R info(@PathVariable("id") Integer id){
		DashboardRecorderManagementEntity dashboardRecorderManagement = dashboardRecorderManagementService.getById(id);

        return R.ok().put("dashboardRecorderManagement", dashboardRecorderManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardrecordermanagement:save")
    public R save(@RequestBody DashboardRecorderManagementEntity dashboardRecorderManagement){
		dashboardRecorderManagementService.save(dashboardRecorderManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardrecordermanagement:update")
    public R update(@RequestBody DashboardRecorderManagementEntity dashboardRecorderManagement){
		dashboardRecorderManagementService.updateById(dashboardRecorderManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardrecordermanagement:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardRecorderManagementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
