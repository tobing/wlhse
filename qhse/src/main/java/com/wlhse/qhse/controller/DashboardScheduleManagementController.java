package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardScheduleManagementEntity;
import com.wlhse.qhse.service.DashboardScheduleManagementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;


/**
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@RestController
@RequestMapping("qhse/dashboardschedulemanagement")
public class DashboardScheduleManagementController {
    @Autowired
    private DashboardScheduleManagementService dashboardScheduleManagementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardschedulemanagement:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = dashboardScheduleManagementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardschedulemanagement:info")
    public R info(@PathVariable("id") Integer id) {
        DashboardScheduleManagementEntity dashboardScheduleManagement = dashboardScheduleManagementService.getById(id);

        return R.ok().put("dashboardScheduleManagement", dashboardScheduleManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardschedulemanagement:save")
    public R save(@RequestBody DashboardScheduleManagementEntity dashboardScheduleManagement) {
        dashboardScheduleManagementService.save(dashboardScheduleManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardschedulemanagement:update")
    public R update(@RequestBody DashboardScheduleManagementEntity dashboardScheduleManagement) {
        dashboardScheduleManagementService.updateById(dashboardScheduleManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardschedulemanagement:delete")
    public R delete(@RequestBody Integer[] ids) {
        dashboardScheduleManagementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
