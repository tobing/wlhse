package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardSecurityMillionEntity;
import com.wlhse.qhse.service.DashboardSecurityMillionService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@RestController
@RequestMapping("qhse/dashboardsecuritymillion")
public class DashboardSecurityMillionController {
    @Autowired
    private DashboardSecurityMillionService dashboardSecurityMillionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardsecuritymillion:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardSecurityMillionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardsecuritymillion:info")
    public R info(@PathVariable("id") Integer id){
		DashboardSecurityMillionEntity dashboardSecurityMillion = dashboardSecurityMillionService.getById(id);

        return R.ok().put("dashboardSecurityMillion", dashboardSecurityMillion);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardsecuritymillion:save")
    public R save(@RequestBody DashboardSecurityMillionEntity dashboardSecurityMillion){
		dashboardSecurityMillionService.save(dashboardSecurityMillion);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardsecuritymillion:update")
    public R update(@RequestBody DashboardSecurityMillionEntity dashboardSecurityMillion){
		dashboardSecurityMillionService.updateById(dashboardSecurityMillion);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardsecuritymillion:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardSecurityMillionService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
