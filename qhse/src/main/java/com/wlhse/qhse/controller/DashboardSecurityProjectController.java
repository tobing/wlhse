package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardSecurityProjectEntity;
import com.wlhse.qhse.service.DashboardSecurityProjectService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@RestController
@RequestMapping("qhse/dashboardsecurityproject")
public class DashboardSecurityProjectController {
    @Autowired
    private DashboardSecurityProjectService dashboardSecurityProjectService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardsecurityproject:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardSecurityProjectService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardsecurityproject:info")
    public R info(@PathVariable("id") Integer id){
		DashboardSecurityProjectEntity dashboardSecurityProject = dashboardSecurityProjectService.getById(id);

        return R.ok().put("dashboardSecurityProject", dashboardSecurityProject);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardsecurityproject:save")
    public R save(@RequestBody DashboardSecurityProjectEntity dashboardSecurityProject){
		dashboardSecurityProjectService.save(dashboardSecurityProject);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardsecurityproject:update")
    public R update(@RequestBody DashboardSecurityProjectEntity dashboardSecurityProject){
		dashboardSecurityProjectService.updateById(dashboardSecurityProject);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardsecurityproject:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardSecurityProjectService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
