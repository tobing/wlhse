package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DashboardSercurityManagementEntity;
import com.wlhse.qhse.service.DashboardSercurityManagementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@RestController
@RequestMapping("qhse/dashboardsercuritymanagement")
public class DashboardSercurityManagementController {
    @Autowired
    private DashboardSercurityManagementService dashboardSercurityManagementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:dashboardsercuritymanagement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dashboardSercurityManagementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:dashboardsercuritymanagement:info")
    public R info(@PathVariable("id") Integer id){
		DashboardSercurityManagementEntity dashboardSercurityManagement = dashboardSercurityManagementService.getById(id);

        return R.ok().put("dashboardSercurityManagement", dashboardSercurityManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:dashboardsercuritymanagement:save")
    public R save(@RequestBody DashboardSercurityManagementEntity dashboardSercurityManagement){
		dashboardSercurityManagementService.save(dashboardSercurityManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:dashboardsercuritymanagement:update")
    public R update(@RequestBody DashboardSercurityManagementEntity dashboardSercurityManagement){
		dashboardSercurityManagementService.updateById(dashboardSercurityManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:dashboardsercuritymanagement:delete")
    public R delete(@RequestBody Integer[] ids){
		dashboardSercurityManagementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
