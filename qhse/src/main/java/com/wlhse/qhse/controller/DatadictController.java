package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.DatadictEntity;
import com.wlhse.qhse.service.DatadictService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/datadict")
public class DatadictController {
    @Autowired
    private DatadictService datadictService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:datadict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = datadictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{datadictId}")
    @RequiresPermissions("qhse:datadict:info")
    public R info(@PathVariable("datadictId") Integer datadictId){
		DatadictEntity datadict = datadictService.getById(datadictId);

        return R.ok().put("datadict", datadict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:datadict:save")
    public R save(@RequestBody DatadictEntity datadict){
		datadictService.save(datadict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:datadict:update")
    public R update(@RequestBody DatadictEntity datadict){
		datadictService.updateById(datadict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:datadict:delete")
    public R delete(@RequestBody Integer[] datadictIds){
		datadictService.removeByIds(Arrays.asList(datadictIds));

        return R.ok();
    }

}
