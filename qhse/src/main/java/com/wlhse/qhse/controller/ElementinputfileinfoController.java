package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ElementinputfileinfoEntity;
import com.wlhse.qhse.service.ElementinputfileinfoService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/elementinputfileinfo")
public class ElementinputfileinfoController {
    @Autowired
    private ElementinputfileinfoService elementinputfileinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:elementinputfileinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = elementinputfileinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{elementFileId}")
    @RequiresPermissions("qhse:elementinputfileinfo:info")
    public R info(@PathVariable("elementFileId") Integer elementFileId){
		ElementinputfileinfoEntity elementinputfileinfo = elementinputfileinfoService.getById(elementFileId);

        return R.ok().put("elementinputfileinfo", elementinputfileinfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:elementinputfileinfo:save")
    public R save(@RequestBody ElementinputfileinfoEntity elementinputfileinfo){
		elementinputfileinfoService.save(elementinputfileinfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:elementinputfileinfo:update")
    public R update(@RequestBody ElementinputfileinfoEntity elementinputfileinfo){
		elementinputfileinfoService.updateById(elementinputfileinfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:elementinputfileinfo:delete")
    public R delete(@RequestBody Integer[] elementFileIds){
		elementinputfileinfoService.removeByIds(Arrays.asList(elementFileIds));

        return R.ok();
    }

}
