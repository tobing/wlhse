package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FactorEntity;
import com.wlhse.qhse.service.FactorService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/factor")
public class FactorController {
    @Autowired
    private FactorService factorService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:factor:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = factorService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:factor:info")
    public R info(@PathVariable("id") Integer id){
		FactorEntity factor = factorService.getById(id);

        return R.ok().put("factor", factor);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:factor:save")
    public R save(@RequestBody FactorEntity factor){
		factorService.save(factor);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:factor:update")
    public R update(@RequestBody FactorEntity factor){
		factorService.updateById(factor);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:factor:delete")
    public R delete(@RequestBody Integer[] ids){
		factorService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
