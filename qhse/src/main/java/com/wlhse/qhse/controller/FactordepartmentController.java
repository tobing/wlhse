package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FactordepartmentEntity;
import com.wlhse.qhse.service.FactordepartmentService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/factordepartment")
public class FactordepartmentController {
    @Autowired
    private FactordepartmentService factordepartmentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:factordepartment:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = factordepartmentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:factordepartment:info")
    public R info(@PathVariable("id") Integer id){
		FactordepartmentEntity factordepartment = factordepartmentService.getById(id);

        return R.ok().put("factordepartment", factordepartment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:factordepartment:save")
    public R save(@RequestBody FactordepartmentEntity factordepartment){
		factordepartmentService.save(factordepartment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:factordepartment:update")
    public R update(@RequestBody FactordepartmentEntity factordepartment){
		factordepartmentService.updateById(factordepartment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:factordepartment:delete")
    public R delete(@RequestBody Integer[] ids){
		factordepartmentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
