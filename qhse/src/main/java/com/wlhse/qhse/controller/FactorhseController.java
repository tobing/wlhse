package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FactorhseEntity;
import com.wlhse.qhse.service.FactorhseService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/factorhse")
public class FactorhseController {
    @Autowired
    private FactorhseService factorhseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:factorhse:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = factorhseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:factorhse:info")
    public R info(@PathVariable("id") Integer id){
		FactorhseEntity factorhse = factorhseService.getById(id);

        return R.ok().put("factorhse", factorhse);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:factorhse:save")
    public R save(@RequestBody FactorhseEntity factorhse){
		factorhseService.save(factorhse);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:factorhse:update")
    public R update(@RequestBody FactorhseEntity factorhse){
		factorhseService.updateById(factorhse);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:factorhse:delete")
    public R delete(@RequestBody Integer[] ids){
		factorhseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
