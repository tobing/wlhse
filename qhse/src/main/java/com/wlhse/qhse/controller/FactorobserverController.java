package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FactorobserverEntity;
import com.wlhse.qhse.service.FactorobserverService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@RestController
@RequestMapping("qhse/factorobserver")
public class FactorobserverController {
    @Autowired
    private FactorobserverService factorobserverService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:factorobserver:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = factorobserverService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:factorobserver:info")
    public R info(@PathVariable("id") Integer id){
		FactorobserverEntity factorobserver = factorobserverService.getById(id);

        return R.ok().put("factorobserver", factorobserver);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:factorobserver:save")
    public R save(@RequestBody FactorobserverEntity factorobserver){
		factorobserverService.save(factorobserver);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:factorobserver:update")
    public R update(@RequestBody FactorobserverEntity factorobserver){
		factorobserverService.updateById(factorobserver);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:factorobserver:delete")
    public R delete(@RequestBody Integer[] ids){
		factorobserverService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
