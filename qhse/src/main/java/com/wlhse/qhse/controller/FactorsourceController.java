package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FactorsourceEntity;
import com.wlhse.qhse.service.FactorsourceService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:57
 */
@RestController
@RequestMapping("qhse/factorsource")
public class FactorsourceController {
    @Autowired
    private FactorsourceService factorsourceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:factorsource:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = factorsourceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:factorsource:info")
    public R info(@PathVariable("id") Integer id){
		FactorsourceEntity factorsource = factorsourceService.getById(id);

        return R.ok().put("factorsource", factorsource);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:factorsource:save")
    public R save(@RequestBody FactorsourceEntity factorsource){
		factorsourceService.save(factorsource);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:factorsource:update")
    public R update(@RequestBody FactorsourceEntity factorsource){
		factorsourceService.updateById(factorsource);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:factorsource:delete")
    public R delete(@RequestBody Integer[] ids){
		factorsourceService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
