package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FilePropagationfileinfoEntity;
import com.wlhse.qhse.service.FilePropagationfileinfoService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@RestController
@RequestMapping("qhse/filepropagationfileinfo")
public class FilePropagationfileinfoController {
    @Autowired
    private FilePropagationfileinfoService filePropagationfileinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:filepropagationfileinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = filePropagationfileinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:filepropagationfileinfo:info")
    public R info(@PathVariable("id") Long id){
		FilePropagationfileinfoEntity filePropagationfileinfo = filePropagationfileinfoService.getById(id);

        return R.ok().put("filePropagationfileinfo", filePropagationfileinfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:filepropagationfileinfo:save")
    public R save(@RequestBody FilePropagationfileinfoEntity filePropagationfileinfo){
		filePropagationfileinfoService.save(filePropagationfileinfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:filepropagationfileinfo:update")
    public R update(@RequestBody FilePropagationfileinfoEntity filePropagationfileinfo){
		filePropagationfileinfoService.updateById(filePropagationfileinfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:filepropagationfileinfo:delete")
    public R delete(@RequestBody Long[] ids){
		filePropagationfileinfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
