package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FilePropagationplanEntity;
import com.wlhse.qhse.service.FilePropagationplanService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@RestController
@RequestMapping("qhse/filepropagationplan")
public class FilePropagationplanController {
    @Autowired
    private FilePropagationplanService filePropagationplanService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:filepropagationplan:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = filePropagationplanService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{filePropagationid}")
    @RequiresPermissions("qhse:filepropagationplan:info")
    public R info(@PathVariable("filePropagationid") Long filePropagationid){
		FilePropagationplanEntity filePropagationplan = filePropagationplanService.getById(filePropagationid);

        return R.ok().put("filePropagationplan", filePropagationplan);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:filepropagationplan:save")
    public R save(@RequestBody FilePropagationplanEntity filePropagationplan){
		filePropagationplanService.save(filePropagationplan);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:filepropagationplan:update")
    public R update(@RequestBody FilePropagationplanEntity filePropagationplan){
		filePropagationplanService.updateById(filePropagationplan);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:filepropagationplan:delete")
    public R delete(@RequestBody Long[] filePropagationids){
		filePropagationplanService.removeByIds(Arrays.asList(filePropagationids));

        return R.ok();
    }

}
