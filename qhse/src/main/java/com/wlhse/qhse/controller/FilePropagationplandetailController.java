package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.FilePropagationplandetailEntity;
import com.wlhse.qhse.service.FilePropagationplandetailService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@RestController
@RequestMapping("qhse/filepropagationplandetail")
public class FilePropagationplandetailController {
    @Autowired
    private FilePropagationplandetailService filePropagationplandetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:filepropagationplandetail:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = filePropagationplandetailService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{filePropagationplandetailid}")
    @RequiresPermissions("qhse:filepropagationplandetail:info")
    public R info(@PathVariable("filePropagationplandetailid") Integer filePropagationplandetailid){
		FilePropagationplandetailEntity filePropagationplandetail = filePropagationplandetailService.getById(filePropagationplandetailid);

        return R.ok().put("filePropagationplandetail", filePropagationplandetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:filepropagationplandetail:save")
    public R save(@RequestBody FilePropagationplandetailEntity filePropagationplandetail){
		filePropagationplandetailService.save(filePropagationplandetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:filepropagationplandetail:update")
    public R update(@RequestBody FilePropagationplandetailEntity filePropagationplandetail){
		filePropagationplandetailService.updateById(filePropagationplandetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:filepropagationplandetail:delete")
    public R delete(@RequestBody Integer[] filePropagationplandetailids){
		filePropagationplandetailService.removeByIds(Arrays.asList(filePropagationplandetailids));

        return R.ok();
    }

}
