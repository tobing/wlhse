package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.InterfaceModuleEntity;
import com.wlhse.qhse.service.InterfaceModuleService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@RestController
@RequestMapping("qhse/interfacemodule")
public class InterfaceModuleController {
    @Autowired
    private InterfaceModuleService interfaceModuleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:interfacemodule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = interfaceModuleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:interfacemodule:info")
    public R info(@PathVariable("id") Integer id){
		InterfaceModuleEntity interfaceModule = interfaceModuleService.getById(id);

        return R.ok().put("interfaceModule", interfaceModule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:interfacemodule:save")
    public R save(@RequestBody InterfaceModuleEntity interfaceModule){
		interfaceModuleService.save(interfaceModule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:interfacemodule:update")
    public R update(@RequestBody InterfaceModuleEntity interfaceModule){
		interfaceModuleService.updateById(interfaceModule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:interfacemodule:delete")
    public R delete(@RequestBody Integer[] ids){
		interfaceModuleService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
