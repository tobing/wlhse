package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MeschecksumdataEntity;
import com.wlhse.qhse.service.MeschecksumdataService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/meschecksumdata")
public class MeschecksumdataController {
    @Autowired
    private MeschecksumdataService meschecksumdataService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:meschecksumdata:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = meschecksumdataService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{meschecksumdataid}")
    @RequiresPermissions("qhse:meschecksumdata:info")
    public R info(@PathVariable("meschecksumdataid") Integer meschecksumdataid){
		MeschecksumdataEntity meschecksumdata = meschecksumdataService.getById(meschecksumdataid);

        return R.ok().put("meschecksumdata", meschecksumdata);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:meschecksumdata:save")
    public R save(@RequestBody MeschecksumdataEntity meschecksumdata){
		meschecksumdataService.save(meschecksumdata);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:meschecksumdata:update")
    public R update(@RequestBody MeschecksumdataEntity meschecksumdata){
		meschecksumdataService.updateById(meschecksumdata);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:meschecksumdata:delete")
    public R delete(@RequestBody Integer[] meschecksumdataids){
		meschecksumdataService.removeByIds(Arrays.asList(meschecksumdataids));

        return R.ok();
    }

}
