package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MessumdateEntity;
import com.wlhse.qhse.service.MessumdateService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/messumdate")
public class MessumdateController {
    @Autowired
    private MessumdateService messumdateService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:messumdate:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = messumdateService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:messumdate:info")
    public R info(@PathVariable("id") Integer id){
		MessumdateEntity messumdate = messumdateService.getById(id);

        return R.ok().put("messumdate", messumdate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:messumdate:save")
    public R save(@RequestBody MessumdateEntity messumdate){
		messumdateService.save(messumdate);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:messumdate:update")
    public R update(@RequestBody MessumdateEntity messumdate){
		messumdateService.updateById(messumdate);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:messumdate:delete")
    public R delete(@RequestBody Integer[] ids){
		messumdateService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
