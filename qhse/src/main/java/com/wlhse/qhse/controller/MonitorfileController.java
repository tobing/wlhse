package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MonitorfileEntity;
import com.wlhse.qhse.service.MonitorfileService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/monitorfile")
public class MonitorfileController {
    @Autowired
    private MonitorfileService monitorfileService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:monitorfile:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = monitorfileService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:monitorfile:info")
    public R info(@PathVariable("id") Integer id){
		MonitorfileEntity monitorfile = monitorfileService.getById(id);

        return R.ok().put("monitorfile", monitorfile);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:monitorfile:save")
    public R save(@RequestBody MonitorfileEntity monitorfile){
		monitorfileService.save(monitorfile);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:monitorfile:update")
    public R update(@RequestBody MonitorfileEntity monitorfile){
		monitorfileService.updateById(monitorfile);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:monitorfile:delete")
    public R delete(@RequestBody Integer[] ids){
		monitorfileService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
