package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MonitorinputcheckrecordEntity;
import com.wlhse.qhse.service.MonitorinputcheckrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/monitorinputcheckrecord")
public class MonitorinputcheckrecordController {
    @Autowired
    private MonitorinputcheckrecordService monitorinputcheckrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:monitorinputcheckrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = monitorinputcheckrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{monitorinputcheckrecordid}")
    @RequiresPermissions("qhse:monitorinputcheckrecord:info")
    public R info(@PathVariable("monitorinputcheckrecordid") Integer monitorinputcheckrecordid){
		MonitorinputcheckrecordEntity monitorinputcheckrecord = monitorinputcheckrecordService.getById(monitorinputcheckrecordid);

        return R.ok().put("monitorinputcheckrecord", monitorinputcheckrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:monitorinputcheckrecord:save")
    public R save(@RequestBody MonitorinputcheckrecordEntity monitorinputcheckrecord){
		monitorinputcheckrecordService.save(monitorinputcheckrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:monitorinputcheckrecord:update")
    public R update(@RequestBody MonitorinputcheckrecordEntity monitorinputcheckrecord){
		monitorinputcheckrecordService.updateById(monitorinputcheckrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:monitorinputcheckrecord:delete")
    public R delete(@RequestBody Integer[] monitorinputcheckrecordids){
		monitorinputcheckrecordService.removeByIds(Arrays.asList(monitorinputcheckrecordids));

        return R.ok();
    }

}
