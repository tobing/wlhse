package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MonitorplanEntity;
import com.wlhse.qhse.service.MonitorplanService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/monitorplan")
public class MonitorplanController {
    @Autowired
    private MonitorplanService monitorplanService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:monitorplan:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = monitorplanService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{monitorplanid}")
    @RequiresPermissions("qhse:monitorplan:info")
    public R info(@PathVariable("monitorplanid") Integer monitorplanid){
		MonitorplanEntity monitorplan = monitorplanService.getById(monitorplanid);

        return R.ok().put("monitorplan", monitorplan);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:monitorplan:save")
    public R save(@RequestBody MonitorplanEntity monitorplan){
		monitorplanService.save(monitorplan);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:monitorplan:update")
    public R update(@RequestBody MonitorplanEntity monitorplan){
		monitorplanService.updateById(monitorplan);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:monitorplan:delete")
    public R delete(@RequestBody Integer[] monitorplanids){
		monitorplanService.removeByIds(Arrays.asList(monitorplanids));

        return R.ok();
    }

}
