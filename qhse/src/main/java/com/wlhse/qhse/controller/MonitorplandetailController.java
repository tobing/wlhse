package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.MonitorplandetailEntity;
import com.wlhse.qhse.service.MonitorplandetailService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/monitorplandetail")
public class MonitorplandetailController {
    @Autowired
    private MonitorplandetailService monitorplandetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:monitorplandetail:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = monitorplandetailService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{monitorplandetailid}")
    @RequiresPermissions("qhse:monitorplandetail:info")
    public R info(@PathVariable("monitorplandetailid") Integer monitorplandetailid){
		MonitorplandetailEntity monitorplandetail = monitorplandetailService.getById(monitorplandetailid);

        return R.ok().put("monitorplandetail", monitorplandetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:monitorplandetail:save")
    public R save(@RequestBody MonitorplandetailEntity monitorplandetail){
		monitorplandetailService.save(monitorplandetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:monitorplandetail:update")
    public R update(@RequestBody MonitorplandetailEntity monitorplandetail){
		monitorplandetailService.updateById(monitorplandetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:monitorplandetail:delete")
    public R delete(@RequestBody Integer[] monitorplandetailids){
		monitorplandetailService.removeByIds(Arrays.asList(monitorplandetailids));

        return R.ok();
    }

}
