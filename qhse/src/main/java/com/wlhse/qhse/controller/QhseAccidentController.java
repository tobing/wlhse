package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseAccidentEntity;
import com.wlhse.qhse.service.QhseAccidentService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@RestController
@RequestMapping("qhse/qhseaccident")
public class QhseAccidentController {
    @Autowired
    private QhseAccidentService qhseAccidentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhseaccident:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseAccidentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{accidentid}")
    @RequiresPermissions("qhse:qhseaccident:info")
    public R info(@PathVariable("accidentid") Integer accidentid){
		QhseAccidentEntity qhseAccident = qhseAccidentService.getById(accidentid);

        return R.ok().put("qhseAccident", qhseAccident);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhseaccident:save")
    public R save(@RequestBody QhseAccidentEntity qhseAccident){
		qhseAccidentService.save(qhseAccident);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhseaccident:update")
    public R update(@RequestBody QhseAccidentEntity qhseAccident){
		qhseAccidentService.updateById(qhseAccident);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhseaccident:delete")
    public R delete(@RequestBody Integer[] accidentids){
		qhseAccidentService.removeByIds(Arrays.asList(accidentids));

        return R.ok();
    }

}
