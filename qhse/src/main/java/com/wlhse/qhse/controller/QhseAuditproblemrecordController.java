package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseAuditproblemrecordEntity;
import com.wlhse.qhse.service.QhseAuditproblemrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/qhseauditproblemrecord")
public class QhseAuditproblemrecordController {
    @Autowired
    private QhseAuditproblemrecordService qhseAuditproblemrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhseauditproblemrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseAuditproblemrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseAuditproblemrecordId}")
    @RequiresPermissions("qhse:qhseauditproblemrecord:info")
    public R info(@PathVariable("qhseAuditproblemrecordId") Integer qhseAuditproblemrecordId){
		QhseAuditproblemrecordEntity qhseAuditproblemrecord = qhseAuditproblemrecordService.getById(qhseAuditproblemrecordId);

        return R.ok().put("qhseAuditproblemrecord", qhseAuditproblemrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhseauditproblemrecord:save")
    public R save(@RequestBody QhseAuditproblemrecordEntity qhseAuditproblemrecord){
		qhseAuditproblemrecordService.save(qhseAuditproblemrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhseauditproblemrecord:update")
    public R update(@RequestBody QhseAuditproblemrecordEntity qhseAuditproblemrecord){
		qhseAuditproblemrecordService.updateById(qhseAuditproblemrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhseauditproblemrecord:delete")
    public R delete(@RequestBody Integer[] qhseAuditproblemrecordIds){
		qhseAuditproblemrecordService.removeByIds(Arrays.asList(qhseAuditproblemrecordIds));

        return R.ok();
    }

}
