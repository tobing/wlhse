package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsecompanyyearmanagersyselement")
public class QhseCompanyyearmanagersyselementController {
    @Autowired
    private QhseCompanyyearmanagersyselementService qhseCompanyyearmanagersyselementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseCompanyyearmanagersyselementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseCompanyyearmanagersyselementId}")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselement:info")
    public R info(@PathVariable("qhseCompanyyearmanagersyselementId") Integer qhseCompanyyearmanagersyselementId){
		QhseCompanyyearmanagersyselementEntity qhseCompanyyearmanagersyselement = qhseCompanyyearmanagersyselementService.getById(qhseCompanyyearmanagersyselementId);

        return R.ok().put("qhseCompanyyearmanagersyselement", qhseCompanyyearmanagersyselement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselement:save")
    public R save(@RequestBody QhseCompanyyearmanagersyselementEntity qhseCompanyyearmanagersyselement){
		qhseCompanyyearmanagersyselementService.save(qhseCompanyyearmanagersyselement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselement:update")
    public R update(@RequestBody QhseCompanyyearmanagersyselementEntity qhseCompanyyearmanagersyselement){
		qhseCompanyyearmanagersyselementService.updateById(qhseCompanyyearmanagersyselement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselement:delete")
    public R delete(@RequestBody Integer[] qhseCompanyyearmanagersyselementIds){
		qhseCompanyyearmanagersyselementService.removeByIds(Arrays.asList(qhseCompanyyearmanagersyselementIds));

        return R.ok();
    }

}
