package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementevidenceEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementevidenceService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsecompanyyearmanagersyselementevidence")
public class QhseCompanyyearmanagersyselementevidenceController {
    @Autowired
    private QhseCompanyyearmanagersyselementevidenceService qhseCompanyyearmanagersyselementevidenceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidence:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseCompanyyearmanagersyselementevidenceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseCompanyyearmanagersyselementevidenceId}")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidence:info")
    public R info(@PathVariable("qhseCompanyyearmanagersyselementevidenceId") Integer qhseCompanyyearmanagersyselementevidenceId){
		QhseCompanyyearmanagersyselementevidenceEntity qhseCompanyyearmanagersyselementevidence = qhseCompanyyearmanagersyselementevidenceService.getById(qhseCompanyyearmanagersyselementevidenceId);

        return R.ok().put("qhseCompanyyearmanagersyselementevidence", qhseCompanyyearmanagersyselementevidence);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidence:save")
    public R save(@RequestBody QhseCompanyyearmanagersyselementevidenceEntity qhseCompanyyearmanagersyselementevidence){
		qhseCompanyyearmanagersyselementevidenceService.save(qhseCompanyyearmanagersyselementevidence);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidence:update")
    public R update(@RequestBody QhseCompanyyearmanagersyselementevidenceEntity qhseCompanyyearmanagersyselementevidence){
		qhseCompanyyearmanagersyselementevidenceService.updateById(qhseCompanyyearmanagersyselementevidence);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidence:delete")
    public R delete(@RequestBody Integer[] qhseCompanyyearmanagersyselementevidenceIds){
		qhseCompanyyearmanagersyselementevidenceService.removeByIds(Arrays.asList(qhseCompanyyearmanagersyselementevidenceIds));

        return R.ok();
    }

}
