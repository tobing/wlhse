package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementevidenceattachEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementevidenceattachService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsecompanyyearmanagersyselementevidenceattach")
public class QhseCompanyyearmanagersyselementevidenceattachController {
    @Autowired
    private QhseCompanyyearmanagersyselementevidenceattachService qhseCompanyyearmanagersyselementevidenceattachService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidenceattach:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseCompanyyearmanagersyselementevidenceattachService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseCompanyyearmanagersyselementevidenceattachId}")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidenceattach:info")
    public R info(@PathVariable("qhseCompanyyearmanagersyselementevidenceattachId") Integer qhseCompanyyearmanagersyselementevidenceattachId){
		QhseCompanyyearmanagersyselementevidenceattachEntity qhseCompanyyearmanagersyselementevidenceattach = qhseCompanyyearmanagersyselementevidenceattachService.getById(qhseCompanyyearmanagersyselementevidenceattachId);

        return R.ok().put("qhseCompanyyearmanagersyselementevidenceattach", qhseCompanyyearmanagersyselementevidenceattach);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidenceattach:save")
    public R save(@RequestBody QhseCompanyyearmanagersyselementevidenceattachEntity qhseCompanyyearmanagersyselementevidenceattach){
		qhseCompanyyearmanagersyselementevidenceattachService.save(qhseCompanyyearmanagersyselementevidenceattach);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidenceattach:update")
    public R update(@RequestBody QhseCompanyyearmanagersyselementevidenceattachEntity qhseCompanyyearmanagersyselementevidenceattach){
		qhseCompanyyearmanagersyselementevidenceattachService.updateById(qhseCompanyyearmanagersyselementevidenceattach);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementevidenceattach:delete")
    public R delete(@RequestBody Integer[] qhseCompanyyearmanagersyselementevidenceattachIds){
		qhseCompanyyearmanagersyselementevidenceattachService.removeByIds(Arrays.asList(qhseCompanyyearmanagersyselementevidenceattachIds));

        return R.ok();
    }

}
