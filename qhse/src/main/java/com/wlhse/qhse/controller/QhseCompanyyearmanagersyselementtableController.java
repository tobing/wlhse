package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementtableEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementtableService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@RestController
@RequestMapping("qhse/qhsecompanyyearmanagersyselementtable")
public class QhseCompanyyearmanagersyselementtableController {
    @Autowired
    private QhseCompanyyearmanagersyselementtableService qhseCompanyyearmanagersyselementtableService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementtable:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseCompanyyearmanagersyselementtableService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseCompanyyearmanagersyselementtableId}")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementtable:info")
    public R info(@PathVariable("qhseCompanyyearmanagersyselementtableId") Integer qhseCompanyyearmanagersyselementtableId){
		QhseCompanyyearmanagersyselementtableEntity qhseCompanyyearmanagersyselementtable = qhseCompanyyearmanagersyselementtableService.getById(qhseCompanyyearmanagersyselementtableId);

        return R.ok().put("qhseCompanyyearmanagersyselementtable", qhseCompanyyearmanagersyselementtable);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementtable:save")
    public R save(@RequestBody QhseCompanyyearmanagersyselementtableEntity qhseCompanyyearmanagersyselementtable){
		qhseCompanyyearmanagersyselementtableService.save(qhseCompanyyearmanagersyselementtable);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementtable:update")
    public R update(@RequestBody QhseCompanyyearmanagersyselementtableEntity qhseCompanyyearmanagersyselementtable){
		qhseCompanyyearmanagersyselementtableService.updateById(qhseCompanyyearmanagersyselementtable);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsecompanyyearmanagersyselementtable:delete")
    public R delete(@RequestBody Integer[] qhseCompanyyearmanagersyselementtableIds){
		qhseCompanyyearmanagersyselementtableService.removeByIds(Arrays.asList(qhseCompanyyearmanagersyselementtableIds));

        return R.ok();
    }

}
