package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseComsyselestatusEntity;
import com.wlhse.qhse.service.QhseComsyselestatusService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/qhsecomsyselestatus")
public class QhseComsyselestatusController {
    @Autowired
    private QhseComsyselestatusService qhseComsyselestatusService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsecomsyselestatus:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseComsyselestatusService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:qhsecomsyselestatus:info")
    public R info(@PathVariable("id") Integer id){
		QhseComsyselestatusEntity qhseComsyselestatus = qhseComsyselestatusService.getById(id);

        return R.ok().put("qhseComsyselestatus", qhseComsyselestatus);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsecomsyselestatus:save")
    public R save(@RequestBody QhseComsyselestatusEntity qhseComsyselestatus){
		qhseComsyselestatusService.save(qhseComsyselestatus);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsecomsyselestatus:update")
    public R update(@RequestBody QhseComsyselestatusEntity qhseComsyselestatus){
		qhseComsyselestatusService.updateById(qhseComsyselestatus);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsecomsyselestatus:delete")
    public R delete(@RequestBody Integer[] ids){
		qhseComsyselestatusService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
