package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseEventEntity;
import com.wlhse.qhse.service.QhseEventService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/qhseevent")
public class QhseEventController {
    @Autowired
    private QhseEventService qhseEventService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhseevent:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseEventService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{eventid}")
    @RequiresPermissions("qhse:qhseevent:info")
    public R info(@PathVariable("eventid") Integer eventid){
		QhseEventEntity qhseEvent = qhseEventService.getById(eventid);

        return R.ok().put("qhseEvent", qhseEvent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhseevent:save")
    public R save(@RequestBody QhseEventEntity qhseEvent){
		qhseEventService.save(qhseEvent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhseevent:update")
    public R update(@RequestBody QhseEventEntity qhseEvent){
		qhseEventService.updateById(qhseEvent);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhseevent:delete")
    public R delete(@RequestBody Integer[] eventids){
		qhseEventService.removeByIds(Arrays.asList(eventids));

        return R.ok();
    }

}
