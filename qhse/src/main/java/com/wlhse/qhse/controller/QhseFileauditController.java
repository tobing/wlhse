package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseFileauditEntity;
import com.wlhse.qhse.service.QhseFileauditService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsefileaudit")
public class QhseFileauditController {
    @Autowired
    private QhseFileauditService qhseFileauditService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsefileaudit:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseFileauditService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseFileauditId}")
    @RequiresPermissions("qhse:qhsefileaudit:info")
    public R info(@PathVariable("qhseFileauditId") Integer qhseFileauditId){
		QhseFileauditEntity qhseFileaudit = qhseFileauditService.getById(qhseFileauditId);

        return R.ok().put("qhseFileaudit", qhseFileaudit);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsefileaudit:save")
    public R save(@RequestBody QhseFileauditEntity qhseFileaudit){
		qhseFileauditService.save(qhseFileaudit);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsefileaudit:update")
    public R update(@RequestBody QhseFileauditEntity qhseFileaudit){
		qhseFileauditService.updateById(qhseFileaudit);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsefileaudit:delete")
    public R delete(@RequestBody Integer[] qhseFileauditIds){
		qhseFileauditService.removeByIds(Arrays.asList(qhseFileauditIds));

        return R.ok();
    }

}
