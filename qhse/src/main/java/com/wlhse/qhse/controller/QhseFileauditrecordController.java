package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseFileauditrecordEntity;
import com.wlhse.qhse.service.QhseFileauditrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsefileauditrecord")
public class QhseFileauditrecordController {
    @Autowired
    private QhseFileauditrecordService qhseFileauditrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsefileauditrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseFileauditrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseFileauditRecordid}")
    @RequiresPermissions("qhse:qhsefileauditrecord:info")
    public R info(@PathVariable("qhseFileauditRecordid") Integer qhseFileauditRecordid){
		QhseFileauditrecordEntity qhseFileauditrecord = qhseFileauditrecordService.getById(qhseFileauditRecordid);

        return R.ok().put("qhseFileauditrecord", qhseFileauditrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsefileauditrecord:save")
    public R save(@RequestBody QhseFileauditrecordEntity qhseFileauditrecord){
		qhseFileauditrecordService.save(qhseFileauditrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsefileauditrecord:update")
    public R update(@RequestBody QhseFileauditrecordEntity qhseFileauditrecord){
		qhseFileauditrecordService.updateById(qhseFileauditrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsefileauditrecord:delete")
    public R delete(@RequestBody Integer[] qhseFileauditRecordids){
		qhseFileauditrecordService.removeByIds(Arrays.asList(qhseFileauditRecordids));

        return R.ok();
    }

}
