package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseManagersyselementEntity;
import com.wlhse.qhse.service.QhseManagersyselementService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@RestController
@RequestMapping("qhse/qhsemanagersyselement")
public class QhseManagersyselementController {
    @Autowired
    private QhseManagersyselementService qhseManagersyselementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsemanagersyselement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseManagersyselementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseManagersyselementId}")
    @RequiresPermissions("qhse:qhsemanagersyselement:info")
    public R info(@PathVariable("qhseManagersyselementId") Integer qhseManagersyselementId){
		QhseManagersyselementEntity qhseManagersyselement = qhseManagersyselementService.getById(qhseManagersyselementId);

        return R.ok().put("qhseManagersyselement", qhseManagersyselement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsemanagersyselement:save")
    public R save(@RequestBody QhseManagersyselementEntity qhseManagersyselement){
		qhseManagersyselementService.save(qhseManagersyselement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsemanagersyselement:update")
    public R update(@RequestBody QhseManagersyselementEntity qhseManagersyselement){
		qhseManagersyselementService.updateById(qhseManagersyselement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsemanagersyselement:delete")
    public R delete(@RequestBody Integer[] qhseManagersyselementIds){
		qhseManagersyselementService.removeByIds(Arrays.asList(qhseManagersyselementIds));

        return R.ok();
    }

}
