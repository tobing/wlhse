package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseManagersyselementproblemdescriptionEntity;
import com.wlhse.qhse.service.QhseManagersyselementproblemdescriptionService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@RestController
@RequestMapping("qhse/qhsemanagersyselementproblemdescription")
public class QhseManagersyselementproblemdescriptionController {
    @Autowired
    private QhseManagersyselementproblemdescriptionService qhseManagersyselementproblemdescriptionService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsemanagersyselementproblemdescription:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseManagersyselementproblemdescriptionService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseManagersyselementproblemdescriptionId}")
    @RequiresPermissions("qhse:qhsemanagersyselementproblemdescription:info")
    public R info(@PathVariable("qhseManagersyselementproblemdescriptionId") Integer qhseManagersyselementproblemdescriptionId){
		QhseManagersyselementproblemdescriptionEntity qhseManagersyselementproblemdescription = qhseManagersyselementproblemdescriptionService.getById(qhseManagersyselementproblemdescriptionId);

        return R.ok().put("qhseManagersyselementproblemdescription", qhseManagersyselementproblemdescription);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsemanagersyselementproblemdescription:save")
    public R save(@RequestBody QhseManagersyselementproblemdescriptionEntity qhseManagersyselementproblemdescription){
		qhseManagersyselementproblemdescriptionService.save(qhseManagersyselementproblemdescription);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsemanagersyselementproblemdescription:update")
    public R update(@RequestBody QhseManagersyselementproblemdescriptionEntity qhseManagersyselementproblemdescription){
		qhseManagersyselementproblemdescriptionService.updateById(qhseManagersyselementproblemdescription);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsemanagersyselementproblemdescription:delete")
    public R delete(@RequestBody Integer[] qhseManagersyselementproblemdescriptionIds){
		qhseManagersyselementproblemdescriptionService.removeByIds(Arrays.asList(qhseManagersyselementproblemdescriptionIds));

        return R.ok();
    }

}
