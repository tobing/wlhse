package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QhseTaskEntity;
import com.wlhse.qhse.service.QhseTaskService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@RestController
@RequestMapping("qhse/qhsetask")
public class QhseTaskController {
    @Autowired
    private QhseTaskService qhseTaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qhsetask:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qhseTaskService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qhseTaskId}")
    @RequiresPermissions("qhse:qhsetask:info")
    public R info(@PathVariable("qhseTaskId") Integer qhseTaskId){
		QhseTaskEntity qhseTask = qhseTaskService.getById(qhseTaskId);

        return R.ok().put("qhseTask", qhseTask);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qhsetask:save")
    public R save(@RequestBody QhseTaskEntity qhseTask){
		qhseTaskService.save(qhseTask);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qhsetask:update")
    public R update(@RequestBody QhseTaskEntity qhseTask){
		qhseTaskService.updateById(qhseTask);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qhsetask:delete")
    public R delete(@RequestBody Integer[] qhseTaskIds){
		qhseTaskService.removeByIds(Arrays.asList(qhseTaskIds));

        return R.ok();
    }

}
