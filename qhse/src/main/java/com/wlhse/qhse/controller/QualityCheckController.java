package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QualityCheckEntity;
import com.wlhse.qhse.service.QualityCheckService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@RestController
@RequestMapping("qhse/qualitycheck")
public class QualityCheckController {
    @Autowired
    private QualityCheckService qualityCheckService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qualitycheck:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qualityCheckService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qulityCheckid}")
    @RequiresPermissions("qhse:qualitycheck:info")
    public R info(@PathVariable("qulityCheckid") Integer qulityCheckid){
		QualityCheckEntity qualityCheck = qualityCheckService.getById(qulityCheckid);

        return R.ok().put("qualityCheck", qualityCheck);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qualitycheck:save")
    public R save(@RequestBody QualityCheckEntity qualityCheck){
		qualityCheckService.save(qualityCheck);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qualitycheck:update")
    public R update(@RequestBody QualityCheckEntity qualityCheck){
		qualityCheckService.updateById(qualityCheck);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qualitycheck:delete")
    public R delete(@RequestBody Integer[] qulityCheckids){
		qualityCheckService.removeByIds(Arrays.asList(qulityCheckids));

        return R.ok();
    }

}
