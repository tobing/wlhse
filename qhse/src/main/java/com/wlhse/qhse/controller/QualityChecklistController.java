package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QualityChecklistEntity;
import com.wlhse.qhse.service.QualityChecklistService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:46
 */
@RestController
@RequestMapping("qhse/qualitychecklist")
public class QualityChecklistController {
    @Autowired
    private QualityChecklistService qualityChecklistService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qualitychecklist:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qualityChecklistService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{checklistid}")
    @RequiresPermissions("qhse:qualitychecklist:info")
    public R info(@PathVariable("checklistid") Integer checklistid){
		QualityChecklistEntity qualityChecklist = qualityChecklistService.getById(checklistid);

        return R.ok().put("qualityChecklist", qualityChecklist);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qualitychecklist:save")
    public R save(@RequestBody QualityChecklistEntity qualityChecklist){
		qualityChecklistService.save(qualityChecklist);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qualitychecklist:update")
    public R update(@RequestBody QualityChecklistEntity qualityChecklist){
		qualityChecklistService.updateById(qualityChecklist);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qualitychecklist:delete")
    public R delete(@RequestBody Integer[] checklistids){
		qualityChecklistService.removeByIds(Arrays.asList(checklistids));

        return R.ok();
    }

}
