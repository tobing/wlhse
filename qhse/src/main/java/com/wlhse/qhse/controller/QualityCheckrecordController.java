package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QualityCheckrecordEntity;
import com.wlhse.qhse.service.QualityCheckrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/qualitycheckrecord")
public class QualityCheckrecordController {
    @Autowired
    private QualityCheckrecordService qualityCheckrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qualitycheckrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qualityCheckrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qulityCheckrecordid}")
    @RequiresPermissions("qhse:qualitycheckrecord:info")
    public R info(@PathVariable("qulityCheckrecordid") Integer qulityCheckrecordid){
		QualityCheckrecordEntity qualityCheckrecord = qualityCheckrecordService.getById(qulityCheckrecordid);

        return R.ok().put("qualityCheckrecord", qualityCheckrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qualitycheckrecord:save")
    public R save(@RequestBody QualityCheckrecordEntity qualityCheckrecord){
		qualityCheckrecordService.save(qualityCheckrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qualitycheckrecord:update")
    public R update(@RequestBody QualityCheckrecordEntity qualityCheckrecord){
		qualityCheckrecordService.updateById(qualityCheckrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qualitycheckrecord:delete")
    public R delete(@RequestBody Integer[] qulityCheckrecordids){
		qualityCheckrecordService.removeByIds(Arrays.asList(qulityCheckrecordids));

        return R.ok();
    }

}
