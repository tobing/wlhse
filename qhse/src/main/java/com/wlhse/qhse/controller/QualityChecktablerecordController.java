package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QualityChecktablerecordEntity;
import com.wlhse.qhse.service.QualityChecktablerecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:46
 */
@RestController
@RequestMapping("qhse/qualitychecktablerecord")
public class QualityChecktablerecordController {
    @Autowired
    private QualityChecktablerecordService qualityChecktablerecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qualitychecktablerecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qualityChecktablerecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{qulityChecktablerecordid}")
    @RequiresPermissions("qhse:qualitychecktablerecord:info")
    public R info(@PathVariable("qulityChecktablerecordid") Integer qulityChecktablerecordid){
		QualityChecktablerecordEntity qualityChecktablerecord = qualityChecktablerecordService.getById(qulityChecktablerecordid);

        return R.ok().put("qualityChecktablerecord", qualityChecktablerecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qualitychecktablerecord:save")
    public R save(@RequestBody QualityChecktablerecordEntity qualityChecktablerecord){
		qualityChecktablerecordService.save(qualityChecktablerecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qualitychecktablerecord:update")
    public R update(@RequestBody QualityChecktablerecordEntity qualityChecktablerecord){
		qualityChecktablerecordService.updateById(qualityChecktablerecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qualitychecktablerecord:delete")
    public R delete(@RequestBody Integer[] qulityChecktablerecordids){
		qualityChecktablerecordService.removeByIds(Arrays.asList(qulityChecktablerecordids));

        return R.ok();
    }

}
