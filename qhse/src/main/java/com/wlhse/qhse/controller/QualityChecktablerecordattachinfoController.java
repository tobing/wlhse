package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.QualityChecktablerecordattachinfoEntity;
import com.wlhse.qhse.service.QualityChecktablerecordattachinfoService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/qualitychecktablerecordattachinfo")
public class QualityChecktablerecordattachinfoController {
    @Autowired
    private QualityChecktablerecordattachinfoService qualityChecktablerecordattachinfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:qualitychecktablerecordattachinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = qualityChecktablerecordattachinfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{tablerecordattachinfoid}")
    @RequiresPermissions("qhse:qualitychecktablerecordattachinfo:info")
    public R info(@PathVariable("tablerecordattachinfoid") Integer tablerecordattachinfoid){
		QualityChecktablerecordattachinfoEntity qualityChecktablerecordattachinfo = qualityChecktablerecordattachinfoService.getById(tablerecordattachinfoid);

        return R.ok().put("qualityChecktablerecordattachinfo", qualityChecktablerecordattachinfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:qualitychecktablerecordattachinfo:save")
    public R save(@RequestBody QualityChecktablerecordattachinfoEntity qualityChecktablerecordattachinfo){
		qualityChecktablerecordattachinfoService.save(qualityChecktablerecordattachinfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:qualitychecktablerecordattachinfo:update")
    public R update(@RequestBody QualityChecktablerecordattachinfoEntity qualityChecktablerecordattachinfo){
		qualityChecktablerecordattachinfoService.updateById(qualityChecktablerecordattachinfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:qualitychecktablerecordattachinfo:delete")
    public R delete(@RequestBody Integer[] tablerecordattachinfoids){
		qualityChecktablerecordattachinfoService.removeByIds(Arrays.asList(tablerecordattachinfoids));

        return R.ok();
    }

}
