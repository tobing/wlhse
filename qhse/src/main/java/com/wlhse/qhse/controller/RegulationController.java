package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.RegulationEntity;
import com.wlhse.qhse.service.RegulationService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/regulation")
public class RegulationController {
    @Autowired
    private RegulationService regulationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:regulation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = regulationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:regulation:info")
    public R info(@PathVariable("id") Integer id){
		RegulationEntity regulation = regulationService.getById(id);

        return R.ok().put("regulation", regulation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:regulation:save")
    public R save(@RequestBody RegulationEntity regulation){
		regulationService.save(regulation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:regulation:update")
    public R update(@RequestBody RegulationEntity regulation){
		regulationService.updateById(regulation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:regulation:delete")
    public R delete(@RequestBody Integer[] ids){
		regulationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
