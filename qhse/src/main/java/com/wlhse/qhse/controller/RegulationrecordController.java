package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.RegulationrecordEntity;
import com.wlhse.qhse.service.RegulationrecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@RestController
@RequestMapping("qhse/regulationrecord")
public class RegulationrecordController {
    @Autowired
    private RegulationrecordService regulationrecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:regulationrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = regulationrecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:regulationrecord:info")
    public R info(@PathVariable("id") Integer id){
		RegulationrecordEntity regulationrecord = regulationrecordService.getById(id);

        return R.ok().put("regulationrecord", regulationrecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:regulationrecord:save")
    public R save(@RequestBody RegulationrecordEntity regulationrecord){
		regulationrecordService.save(regulationrecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:regulationrecord:update")
    public R update(@RequestBody RegulationrecordEntity regulationrecord){
		regulationrecordService.updateById(regulationrecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:regulationrecord:delete")
    public R delete(@RequestBody Integer[] ids){
		regulationrecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
