package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportCodeRuleEntity;
import com.wlhse.qhse.service.ReportCodeRuleService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/reportcoderule")
public class ReportCodeRuleController {
    @Autowired
    private ReportCodeRuleService reportCodeRuleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reportcoderule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportCodeRuleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{reportcoderuleid}")
    @RequiresPermissions("qhse:reportcoderule:info")
    public R info(@PathVariable("reportcoderuleid") Integer reportcoderuleid){
		ReportCodeRuleEntity reportCodeRule = reportCodeRuleService.getById(reportcoderuleid);

        return R.ok().put("reportCodeRule", reportCodeRule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reportcoderule:save")
    public R save(@RequestBody ReportCodeRuleEntity reportCodeRule){
		reportCodeRuleService.save(reportCodeRule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reportcoderule:update")
    public R update(@RequestBody ReportCodeRuleEntity reportCodeRule){
		reportCodeRuleService.updateById(reportCodeRule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reportcoderule:delete")
    public R delete(@RequestBody Integer[] reportcoderuleids){
		reportCodeRuleService.removeByIds(Arrays.asList(reportcoderuleids));

        return R.ok();
    }

}
