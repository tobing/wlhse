package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportEntity;
import com.wlhse.qhse.service.ReportService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/report")
public class ReportController {
    @Autowired
    private ReportService reportService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:report:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{reportid}")
    @RequiresPermissions("qhse:report:info")
    public R info(@PathVariable("reportid") Integer reportid){
		ReportEntity report = reportService.getById(reportid);

        return R.ok().put("report", report);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:report:save")
    public R save(@RequestBody ReportEntity report){
		reportService.save(report);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:report:update")
    public R update(@RequestBody ReportEntity report){
		reportService.updateById(report);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:report:delete")
    public R delete(@RequestBody Integer[] reportids){
		reportService.removeByIds(Arrays.asList(reportids));

        return R.ok();
    }

}
