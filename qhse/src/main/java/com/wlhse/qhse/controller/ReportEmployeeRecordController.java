package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportEmployeeRecordEntity;
import com.wlhse.qhse.service.ReportEmployeeRecordService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@RestController
@RequestMapping("qhse/reportemployeerecord")
public class ReportEmployeeRecordController {
    @Autowired
    private ReportEmployeeRecordService reportEmployeeRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reportemployeerecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportEmployeeRecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("qhse:reportemployeerecord:info")
    public R info(@PathVariable("id") Integer id){
		ReportEmployeeRecordEntity reportEmployeeRecord = reportEmployeeRecordService.getById(id);

        return R.ok().put("reportEmployeeRecord", reportEmployeeRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reportemployeerecord:save")
    public R save(@RequestBody ReportEmployeeRecordEntity reportEmployeeRecord){
		reportEmployeeRecordService.save(reportEmployeeRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reportemployeerecord:update")
    public R update(@RequestBody ReportEmployeeRecordEntity reportEmployeeRecord){
		reportEmployeeRecordService.updateById(reportEmployeeRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reportemployeerecord:delete")
    public R delete(@RequestBody Integer[] ids){
		reportEmployeeRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
