package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportSampleEntity;
import com.wlhse.qhse.service.ReportSampleService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@RestController
@RequestMapping("qhse/reportsample")
public class ReportSampleController {
    @Autowired
    private ReportSampleService reportSampleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reportsample:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportSampleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sampleid}")
    @RequiresPermissions("qhse:reportsample:info")
    public R info(@PathVariable("sampleid") Integer sampleid){
		ReportSampleEntity reportSample = reportSampleService.getById(sampleid);

        return R.ok().put("reportSample", reportSample);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reportsample:save")
    public R save(@RequestBody ReportSampleEntity reportSample){
		reportSampleService.save(reportSample);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reportsample:update")
    public R update(@RequestBody ReportSampleEntity reportSample){
		reportSampleService.updateById(reportSample);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reportsample:delete")
    public R delete(@RequestBody Integer[] sampleids){
		reportSampleService.removeByIds(Arrays.asList(sampleids));

        return R.ok();
    }

}
