package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportStandardEntity;
import com.wlhse.qhse.service.ReportStandardService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@RestController
@RequestMapping("qhse/reportstandard")
public class ReportStandardController {
    @Autowired
    private ReportStandardService reportStandardService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reportstandard:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportStandardService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{standardid}")
    @RequiresPermissions("qhse:reportstandard:info")
    public R info(@PathVariable("standardid") Integer standardid){
		ReportStandardEntity reportStandard = reportStandardService.getById(standardid);

        return R.ok().put("reportStandard", reportStandard);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reportstandard:save")
    public R save(@RequestBody ReportStandardEntity reportStandard){
		reportStandardService.save(reportStandard);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reportstandard:update")
    public R update(@RequestBody ReportStandardEntity reportStandard){
		reportStandardService.updateById(reportStandard);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reportstandard:delete")
    public R delete(@RequestBody Integer[] standardids){
		reportStandardService.removeByIds(Arrays.asList(standardids));

        return R.ok();
    }

}
