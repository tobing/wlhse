package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportTotalCostEntity;
import com.wlhse.qhse.service.ReportTotalCostService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/reporttotalcost")
public class ReportTotalCostController {
    @Autowired
    private ReportTotalCostService reportTotalCostService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reporttotalcost:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportTotalCostService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{costid}")
    @RequiresPermissions("qhse:reporttotalcost:info")
    public R info(@PathVariable("costid") Integer costid){
		ReportTotalCostEntity reportTotalCost = reportTotalCostService.getById(costid);

        return R.ok().put("reportTotalCost", reportTotalCost);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reporttotalcost:save")
    public R save(@RequestBody ReportTotalCostEntity reportTotalCost){
		reportTotalCostService.save(reportTotalCost);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reporttotalcost:update")
    public R update(@RequestBody ReportTotalCostEntity reportTotalCost){
		reportTotalCostService.updateById(reportTotalCost);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reporttotalcost:delete")
    public R delete(@RequestBody Integer[] costids){
		reportTotalCostService.removeByIds(Arrays.asList(costids));

        return R.ok();
    }

}
