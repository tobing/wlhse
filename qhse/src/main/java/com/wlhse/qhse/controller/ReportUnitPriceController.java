package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.ReportUnitPriceEntity;
import com.wlhse.qhse.service.ReportUnitPriceService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/reportunitprice")
public class ReportUnitPriceController {
    @Autowired
    private ReportUnitPriceService reportUnitPriceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:reportunitprice:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = reportUnitPriceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{unitpriceid}")
    @RequiresPermissions("qhse:reportunitprice:info")
    public R info(@PathVariable("unitpriceid") Integer unitpriceid){
		ReportUnitPriceEntity reportUnitPrice = reportUnitPriceService.getById(unitpriceid);

        return R.ok().put("reportUnitPrice", reportUnitPrice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:reportunitprice:save")
    public R save(@RequestBody ReportUnitPriceEntity reportUnitPrice){
		reportUnitPriceService.save(reportUnitPrice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:reportunitprice:update")
    public R update(@RequestBody ReportUnitPriceEntity reportUnitPrice){
		reportUnitPriceService.updateById(reportUnitPrice);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:reportunitprice:delete")
    public R delete(@RequestBody Integer[] unitpriceids){
		reportUnitPriceService.removeByIds(Arrays.asList(unitpriceids));

        return R.ok();
    }

}
