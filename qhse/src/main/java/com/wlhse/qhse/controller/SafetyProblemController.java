package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.SafetyProblemEntity;
import com.wlhse.qhse.service.SafetyProblemService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/safetyproblem")
public class SafetyProblemController {
    @Autowired
    private SafetyProblemService safetyProblemService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:safetyproblem:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = safetyProblemService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{problemid}")
    @RequiresPermissions("qhse:safetyproblem:info")
    public R info(@PathVariable("problemid") Integer problemid){
		SafetyProblemEntity safetyProblem = safetyProblemService.getById(problemid);

        return R.ok().put("safetyProblem", safetyProblem);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:safetyproblem:save")
    public R save(@RequestBody SafetyProblemEntity safetyProblem){
		safetyProblemService.save(safetyProblem);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:safetyproblem:update")
    public R update(@RequestBody SafetyProblemEntity safetyProblem){
		safetyProblemService.updateById(safetyProblem);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:safetyproblem:delete")
    public R delete(@RequestBody Integer[] problemids){
		safetyProblemService.removeByIds(Arrays.asList(problemids));

        return R.ok();
    }

}
