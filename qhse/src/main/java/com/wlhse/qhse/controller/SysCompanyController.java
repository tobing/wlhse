package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.SysCompanyEntity;
import com.wlhse.qhse.service.SysCompanyService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/syscompany")
public class SysCompanyController {
    @Autowired
    private SysCompanyService sysCompanyService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:syscompany:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysCompanyService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sysCompanyid}")
    @RequiresPermissions("qhse:syscompany:info")
    public R info(@PathVariable("sysCompanyid") Integer sysCompanyid){
		SysCompanyEntity sysCompany = sysCompanyService.getById(sysCompanyid);

        return R.ok().put("sysCompany", sysCompany);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:syscompany:save")
    public R save(@RequestBody SysCompanyEntity sysCompany){
		sysCompanyService.save(sysCompany);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:syscompany:update")
    public R update(@RequestBody SysCompanyEntity sysCompany){
		sysCompanyService.updateById(sysCompany);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:syscompany:delete")
    public R delete(@RequestBody Integer[] sysCompanyids){
		sysCompanyService.removeByIds(Arrays.asList(sysCompanyids));

        return R.ok();
    }

}
