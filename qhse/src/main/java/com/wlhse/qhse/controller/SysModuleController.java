package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.SysModuleEntity;
import com.wlhse.qhse.service.SysModuleService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/sysmodule")
public class SysModuleController {
    @Autowired
    private SysModuleService sysModuleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:sysmodule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysModuleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sysModuleId}")
    @RequiresPermissions("qhse:sysmodule:info")
    public R info(@PathVariable("sysModuleId") Integer sysModuleId){
		SysModuleEntity sysModule = sysModuleService.getById(sysModuleId);

        return R.ok().put("sysModule", sysModule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:sysmodule:save")
    public R save(@RequestBody SysModuleEntity sysModule){
		sysModuleService.save(sysModule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:sysmodule:update")
    public R update(@RequestBody SysModuleEntity sysModule){
		sysModuleService.updateById(sysModule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:sysmodule:delete")
    public R delete(@RequestBody Integer[] sysModuleIds){
		sysModuleService.removeByIds(Arrays.asList(sysModuleIds));

        return R.ok();
    }

}
