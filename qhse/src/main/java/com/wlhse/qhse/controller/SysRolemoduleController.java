package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.SysRolemoduleEntity;
import com.wlhse.qhse.service.SysRolemoduleService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@RestController
@RequestMapping("qhse/sysrolemodule")
public class SysRolemoduleController {
    @Autowired
    private SysRolemoduleService sysRolemoduleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:sysrolemodule:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRolemoduleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{sysRolemoduleId}")
    @RequiresPermissions("qhse:sysrolemodule:info")
    public R info(@PathVariable("sysRolemoduleId") Integer sysRolemoduleId){
		SysRolemoduleEntity sysRolemodule = sysRolemoduleService.getById(sysRolemoduleId);

        return R.ok().put("sysRolemodule", sysRolemodule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:sysrolemodule:save")
    public R save(@RequestBody SysRolemoduleEntity sysRolemodule){
		sysRolemoduleService.save(sysRolemodule);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:sysrolemodule:update")
    public R update(@RequestBody SysRolemoduleEntity sysRolemodule){
		sysRolemoduleService.updateById(sysRolemodule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:sysrolemodule:delete")
    public R delete(@RequestBody Integer[] sysRolemoduleIds){
		sysRolemoduleService.removeByIds(Arrays.asList(sysRolemoduleIds));

        return R.ok();
    }

}
