package com.wlhse.qhse.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wlhse.qhse.entity.SysRolesEntity;
import com.wlhse.qhse.service.SysRolesService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.R;



/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@RestController
@RequestMapping("qhse/sysroles")
public class SysRolesController {
    @Autowired
    private SysRolesService sysRolesService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("qhse:sysroles:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysRolesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{roleId}")
    @RequiresPermissions("qhse:sysroles:info")
    public R info(@PathVariable("roleId") Integer roleId){
		SysRolesEntity sysRoles = sysRolesService.getById(roleId);

        return R.ok().put("sysRoles", sysRoles);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("qhse:sysroles:save")
    public R save(@RequestBody SysRolesEntity sysRoles){
		sysRolesService.save(sysRoles);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("qhse:sysroles:update")
    public R update(@RequestBody SysRolesEntity sysRoles){
		sysRolesService.updateById(sysRoles);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("qhse:sysroles:delete")
    public R delete(@RequestBody Integer[] roleIds){
		sysRolesService.removeByIds(Arrays.asList(roleIds));

        return R.ok();
    }

}
