package com.wlhse.qhse.dao;

import com.wlhse.qhse.entity.DatadictEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@Mapper
public interface DatadictDao extends BaseMapper<DatadictEntity> {
	
}
