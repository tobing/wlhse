package com.wlhse.qhse.dao;

import com.wlhse.qhse.entity.FileEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@Mapper
public interface FileDao extends BaseMapper<FileEntity> {
	
}
