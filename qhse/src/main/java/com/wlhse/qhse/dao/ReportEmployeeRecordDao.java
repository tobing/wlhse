package com.wlhse.qhse.dao;

import com.wlhse.qhse.entity.ReportEmployeeRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@Mapper
public interface ReportEmployeeRecordDao extends BaseMapper<ReportEmployeeRecordEntity> {
	
}
