package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@Data
@TableName("checklist")
public class ChecklistEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer checklistid;
	/**
	 * 
	 */
	private String checklistcode;
	/**
	 * 
	 */
	private String checklistname;
	/**
	 * 
	 */
	private String attribute;
	/**
	 * 
	 */
	private String parentname;
	/**
	 * 
	 */
	private String ischildnode;
	/**
	 * 
	 */
	private String status;

}
