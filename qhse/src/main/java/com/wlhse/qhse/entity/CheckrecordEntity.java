package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:57
 */
@Data
@TableName("checkrecord")
public class CheckrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer checkrecordid;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String checktype;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String checkdate;
	/**
	 * 
	 */
	private String pass;
	/**
	 * 
	 */
	private String checktypecode;
	/**
	 * 
	 */
	private String problems;
	/**
	 * 
	 */
	private String checkpersonid;
	/**
	 * 
	 */
	private String checkperson;

}
