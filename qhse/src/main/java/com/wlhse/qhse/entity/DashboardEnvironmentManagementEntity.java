package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@Data
@TableName("dashboard_environment_management")
public class DashboardEnvironmentManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private Double sewagevolume;
	/**
	 * 
	 */
	private Double sewagetransfer;
	/**
	 * 
	 */
	private Double water;
	/**
	 * 
	 */
	private Double electricity;
	/**
	 * 
	 */
	private Double gas;
	/**
	 * 
	 */
	private Double gasoline;
	/**
	 * 
	 */
	private Double diesel;
	/**
	 * 
	 */
	private Date updatetime;

}
