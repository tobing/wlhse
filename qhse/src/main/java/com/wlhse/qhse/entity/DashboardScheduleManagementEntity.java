package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@Data
@TableName("dashboard_schedule_management")
public class DashboardScheduleManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private Integer plannum;
	/**
	 * 
	 */
	private Integer firstdraftfinishnum;
	/**
	 * 
	 */
	private Float firstdraftfinishrate;
	/**
	 * 
	 */
	private Integer reviewpassnum;
	/**
	 * 
	 */
	private Float reviewpassrate;
	/**
	 * 
	 */
	private Integer standardreleasenum;
	/**
	 * 
	 */
	private Float standardreleaserate;
	/**
	 * 
	 */
	private Date updatetime;

}
