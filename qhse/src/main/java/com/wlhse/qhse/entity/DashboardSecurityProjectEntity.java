package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@Data
@TableName("dashboard_security_project")
public class DashboardSecurityProjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String projectlevel;
	/**
	 * 
	 */
	private String projectname;
	/**
	 * 
	 */
	private Double projectfunds;
	/**
	 * 
	 */
	private Double recordedfunds;
	/**
	 * 
	 */
	private Double recordedfundsrate;
	/**
	 * 
	 */
	private Date updatetime;

}
