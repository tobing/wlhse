package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@Data
@TableName("dashboard_sercurity_management")
public class DashboardSercurityManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private Integer quarterdangerindex;
	/**
	 * 
	 */
	private Integer actualfinishdanger;
	/**
	 * 
	 */
	private Integer quarterregulationindex;
	/**
	 * 
	 */
	private Integer actualfinishregulation;
	/**
	 * 
	 */
	private Integer quartereventindex;
	/**
	 * 
	 */
	private Integer actualfinishevent;
	/**
	 * 
	 */
	private Double finishregulationrate;
	/**
	 * 
	 */
	private Double finishdangerrate;
	/**
	 * 
	 */
	private Double eventfinishrate;
	/**
	 * 
	 */
	private Date updatetime;

}
