package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@Data
@TableName("employee")
public class EmployeeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer employeeid;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String employeecode;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String sex;
	/**
	 * 
	 */
	private String birthday;
	/**
	 * 
	 */
	private Date jobtime;
	/**
	 * 
	 */
	private String empgroup;
	/**
	 * 
	 */
	private String position;
	/**
	 * 
	 */
	private String station;
	/**
	 * 
	 */
	private String education;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private String tel;
	/**
	 * 
	 */
	private String category;
	/**
	 * 
	 */
	private String hash;

}
