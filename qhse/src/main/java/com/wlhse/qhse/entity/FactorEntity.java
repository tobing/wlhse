package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@Data
@TableName("factor")
public class FactorEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String factorcode;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String rigth;
	/**
	 * 
	 */
	private String parentid;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String mark;
	/**
	 * 
	 */
	private String factorhsecode;
	/**
	 * 
	 */
	private String factorobservercode;
	/**
	 * 
	 */
	private String factorsourcecode;
	/**
	 * 
	 */
	private String factordepartmentcode;
	/**
	 * 
	 */
	private Integer factorid;

}
