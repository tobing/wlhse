package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:56
 */
@Data
@TableName("factorobserver")
public class FactorobserverEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String factorobservercode1;
	/**
	 * 
	 */
	private String factorobservername1;
	/**
	 * 
	 */
	private String factorobservercode2;
	/**
	 * 
	 */
	private String factorobservername2;
	/**
	 * 
	 */
	private Integer factorobserverid;

}
