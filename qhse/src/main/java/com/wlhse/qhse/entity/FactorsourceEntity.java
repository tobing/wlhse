package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:57
 */
@Data
@TableName("factorsource")
public class FactorsourceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String category;
	/**
	 * 
	 */
	private String factorsourcecode;
	/**
	 * 
	 */
	private String factorsourcename;
	/**
	 * 
	 */
	private Integer factorsourceid;

}
