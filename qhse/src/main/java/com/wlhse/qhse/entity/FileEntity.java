package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@Data
@TableName("file")
public class FileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer fileid;
	/**
	 * 
	 */
	private String filename;
	/**
	 * 
	 */
	private String filetype;
	/**
	 * 
	 */
	private Date uploaddate;
	/**
	 * 
	 */
	private String uploadperson;
	/**
	 * 
	 */
	private String applicomcode;
	/**
	 * 
	 */
	private String summary;
	/**
	 * 
	 */
	private String applicom;
	/**
	 * 
	 */
	private String filenamecode;

}
