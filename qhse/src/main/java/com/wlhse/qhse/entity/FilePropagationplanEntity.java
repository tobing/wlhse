package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@Data
@TableName("file_propagationplan")
public class FilePropagationplanEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long filePropagationid;
	/**
	 * 
	 */
	private String filename;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private Date propagationdate;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private Integer staffid;
	/**
	 * 
	 */
	private String staffname;

}
