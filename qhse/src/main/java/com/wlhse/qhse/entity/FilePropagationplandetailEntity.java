package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:55
 */
@Data
@TableName("file_propagationplandetail")
public class FilePropagationplandetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer filePropagationplandetailid;
	/**
	 * 
	 */
	private Long filePropagationid;
	/**
	 * 
	 */
	private String pushcompanycode;
	/**
	 * 
	 */
	private String pushcompanyname;
	/**
	 * 
	 */
	private Integer pushstaffid;
	/**
	 * 
	 */
	private String pushstaffname;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private Date readdate;
	/**
	 * 
	 */
	private String department;
	/**
	 * 
	 */
	private Date createtime;

}
