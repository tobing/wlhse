package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@Data
@TableName("meschecksumdata")
public class MeschecksumdataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer meschecksumdataid;
	/**
	 * 
	 */
	private Date sumdate;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private Integer worknum;
	/**
	 * 
	 */
	private Integer dayreportnum;
	/**
	 * 
	 */
	private Integer recorddevicenum;
	/**
	 * 
	 */
	private Integer poweronnum;
	/**
	 * 
	 */
	private Integer backnum;
	/**
	 * 
	 */
	private Double coveragerate;
	/**
	 * 
	 */
	private Double userate;
	/**
	 * 
	 */
	private Double avairate;
	/**
	 * 
	 */
	private Integer outstocknum;

}
