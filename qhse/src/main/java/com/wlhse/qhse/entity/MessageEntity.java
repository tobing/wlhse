package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@Data
@TableName("message")
public class MessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer senderid;
	/**
	 * 
	 */
	private Integer receiverid;
	/**
	 * 
	 */
	private String tittle;
	/**
	 * 
	 */
	private String body;
	/**
	 * 
	 */
	private String source;
	/**
	 * 
	 */
	private Date time;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private Date readtime;
	/**
	 * 
	 */
	private Integer deleted;

}
