package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@Data
@TableName("monitorinputcheckrecord")
public class MonitorinputcheckrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer monitorinputcheckrecordid;
	/**
	 * 
	 */
	private Integer monitorplanid;
	/**
	 * 
	 */
	private Integer monitorplandetailid;
	/**
	 * 
	 */
	private String condition;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String picno;
	/**
	 * 
	 */
	private String disposeIn;
	/**
	 * 
	 */
	private String closeIn;
	/**
	 * 
	 */
	private Integer inputpersonid;
	/**
	 * 
	 */
	private String inputpersonname;
	/**
	 * 
	 */
	private Date inputdate;
	/**
	 * 
	 */
	private String checkstatus;
	/**
	 * 
	 */
	private String piclink;
	/**
	 * 
	 */
	private String check;
	/**
	 * 
	 */
	private String result;
	/**
	 * 
	 */
	private String disposeCheck;
	/**
	 * 
	 */
	private String closeCheck;
	/**
	 * 
	 */
	private Date checkdate;
	/**
	 * 
	 */
	private String checkpersonname;
	/**
	 * 
	 */
	private Integer checkpersonid;

}
