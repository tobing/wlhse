package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@Data
@TableName("monitorplan")
public class MonitorplanEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer monitorplanid;
	/**
	 * 
	 */
	private String startdate;
	/**
	 * 
	 */
	private String enddate;
	/**
	 * 
	 */
	private String planname;
	/**
	 * 
	 */
	private Integer planpersonid;
	/**
	 * 
	 */
	private String planpersonname;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private Date createtime;
	/**
	 * 
	 */
	private String status;

}
