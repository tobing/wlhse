package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@Data
@TableName("monitorplandetail")
public class MonitorplandetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer monitorplandetailid;
	/**
	 * 
	 */
	private Integer monitorplanid;
	/**
	 * 
	 */
	private String no;
	/**
	 * 
	 */
	private String deviceno;
	/**
	 * 
	 */
	private String myno;
	/**
	 * 
	 */
	private String projectname;
	/**
	 * 
	 */
	private String charger;
	/**
	 * 
	 */
	private String tel;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String itemcategory;
	/**
	 * 
	 */
	private String projectprogress;
	/**
	 * 
	 */
	private String status;

}
