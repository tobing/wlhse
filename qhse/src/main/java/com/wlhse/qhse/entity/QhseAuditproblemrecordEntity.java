package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@Data
@TableName("qhse_auditproblemrecord")
public class QhseAuditproblemrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseAuditproblemrecordId;
	/**
	 * 
	 */
	private Integer qhseFileauditId;
	/**
	 * 
	 */
	private Integer qhseFileauditrecordId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String problemdescription;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String additor;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String itemname;
	/**
	 * 
	 */
	private Date audittime;
	/**
	 * 
	 */
	private String problemsource;
	/**
	 * 
	 */
	private String situation;
	/**
	 * 
	 */
	private String passreason;
	/**
	 * 
	 */
	private String refusereason;
	/**
	 * 
	 */
	private String file1;
	/**
	 * 
	 */
	private String file2;

}
