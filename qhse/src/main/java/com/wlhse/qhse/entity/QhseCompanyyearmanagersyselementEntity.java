package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@Data
@TableName("qhse_companyyearmanagersyselement")
public class QhseCompanyyearmanagersyselementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseCompanyyearmanagersyselementId;
	/**
	 * 
	 */
	private Integer qhseCompanyyearmanagersyselementtableId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String content;
	/**
	 * 
	 */
	private String auditmode;
	/**
	 * 
	 */
	private Integer initialscore;
	/**
	 * 
	 */
	private String formula;
	/**
	 * 
	 */
	private Integer totalcount;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String year;
	/**
	 * 
	 */
	private String filecheckstatus;
	/**
	 * 
	 */
	private String configstatus;
	/**
	 * 
	 */
	private Integer checkstatus;
	/**
	 * 
	 */
	private Integer isinvolve;
	/**
	 * 
	 */
	private Integer checkpersonid;

}
