package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@Data
@TableName("qhse_companyyearmanagersyselementevidence")
public class QhseCompanyyearmanagersyselementevidenceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseCompanyyearmanagersyselementevidenceId;
	/**
	 * 
	 */
	private Integer qhseCompanyyearmanagersyselementId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String evidencedescription;
	/**
	 * 
	 */
	private Integer checkstaffid;
	/**
	 * 
	 */
	private String checkstaffname;
	/**
	 * 
	 */
	private Integer approverstaffid;
	/**
	 * 
	 */
	private String approverstaffname;
	/**
	 * 
	 */
	private String negativeopinion;

}
