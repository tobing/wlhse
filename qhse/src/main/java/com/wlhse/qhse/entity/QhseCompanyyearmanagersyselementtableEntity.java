package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
@Data
@TableName("qhse_companyyearmanagersyselementtable")
public class QhseCompanyyearmanagersyselementtableEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseCompanyyearmanagersyselementtableId;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String year;
	/**
	 * 
	 */
	private String elementtablename;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private Integer issuedid;
	/**
	 * 
	 */
	private Integer employeeid;

}
