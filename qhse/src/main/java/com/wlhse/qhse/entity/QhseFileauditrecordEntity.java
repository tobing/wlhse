package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
@Data
@TableName("qhse_fileauditrecord")
public class QhseFileauditrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseFileauditRecordid;
	/**
	 * 
	 */
	private Integer qhseFileauditId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String codescore;
	/**
	 * 
	 */
	private String pass;
	/**
	 * 
	 */
	private String additor;
	/**
	 * 
	 */
	private Date audittime;
	/**
	 * 
	 */
	private String nopassreason;

}
