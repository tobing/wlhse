package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:47
 */
@Data
@TableName("qhse_managersyselement")
public class QhseManagersyselementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qhseManagersyselementId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String content;
	/**
	 * 
	 */
	private String auditmode;
	/**
	 * 
	 */
	private Integer initialscore;
	/**
	 * 
	 */
	private String formula;
	/**
	 * 
	 */
	private Integer totalcount;
	/**
	 * 
	 */
	private String status;

}
