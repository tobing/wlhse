package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:50
 */
@Data
@TableName("quality_check")
public class QualityCheckEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qulityCheckid;
	/**
	 * 
	 */
	private String tasktype;
	/**
	 * 
	 */
	private String checkcategory;
	/**
	 * 
	 */
	private String checkbasis;
	/**
	 * 
	 */
	private String checkmethod;
	/**
	 * 
	 */
	private String qualitycheckname;
	/**
	 * 
	 */
	private String checklistname;
	/**
	 * 
	 */
	private String checklistcode;
	/**
	 * 
	 */
	private String checkedcompanyname;
	/**
	 * 
	 */
	private String checkedcompanycode;
	/**
	 * 
	 */
	private String group;
	/**
	 * 
	 */
	private Integer checkedpersonid;
	/**
	 * 
	 */
	private String checkedpersonname;
	/**
	 * 
	 */
	private Integer groupleaderid;
	/**
	 * 
	 */
	private String groupleadername;
	/**
	 * 
	 */
	private String responsicompanyname;
	/**
	 * 
	 */
	private String responsicompanycode;
	/**
	 * 
	 */
	private Integer responsepersonid;
	/**
	 * 
	 */
	private String responsepersonname;
	/**
	 * 
	 */
	private Date checkdate;
	/**
	 * 
	 */
	private Integer checkpersonid;
	/**
	 * 
	 */
	private String checkperson;
	/**
	 * 
	 */
	private String projectname;
	/**
	 * 
	 */
	private Integer projectleaderid;
	/**
	 * 
	 */
	private String projectleadername;
	/**
	 * 
	 */
	private String checkproject;
	/**
	 * 
	 */
	private String execstd;
	/**
	 * 
	 */
	private String contractor;
	/**
	 * 
	 */
	private String owner;
	/**
	 * 
	 */
	private Integer workernumber;
	/**
	 * 
	 */
	private Integer erpnumber;
	/**
	 * 
	 */
	private Integer externalnumber;
	/**
	 * 
	 */
	private String workplace;
	/**
	 * 
	 */
	private Integer contractornumber;
	/**
	 * 
	 */
	private String progress;
	/**
	 * 
	 */
	private Date finishdate;
	/**
	 * 
	 */
	private String ispush;
	/**
	 * 
	 */
	private String issued;

}
