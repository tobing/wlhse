package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@Data
@TableName("quality_checkrecord")
public class QualityCheckrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qulityCheckrecordid;
	/**
	 * 
	 */
	private Integer qulityCheckid;
	/**
	 * 
	 */
	private String checklistcode;
	/**
	 * 
	 */
	private String no;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String nature;
	/**
	 * 
	 */
	private String nonconformitytype;
	/**
	 * 
	 */
	private String nonconformitynature;
	/**
	 * 
	 */
	private String nonconformitystd;
	/**
	 * 
	 */
	private String nonconformitystdno;
	/**
	 * 
	 */
	private String nonconformitystdcontent;
	/**
	 * 
	 */
	private String nonconformclause;
	/**
	 * 
	 */
	private String nonconformclauseno;
	/**
	 * 
	 */
	private String nonconformclausecontent;
	/**
	 * 
	 */
	private String nonconformsource;
	/**
	 * 
	 */
	private String nonconformcorrect;
	/**
	 * 
	 */
	private String nonconformcorrectmeasure;
	/**
	 * 
	 */
	private String nonconformcorrectmeasureverify;
	/**
	 * 
	 */
	private String punishmentbasis;
	/**
	 * 
	 */
	private String violationclause;
	/**
	 * 
	 */
	private String violationclausecontent;
	/**
	 * 
	 */
	private Float violationdeduction;
	/**
	 * 
	 */
	private Float violationscore;
	/**
	 * 
	 */
	private String illegalperson;
	/**
	 * 
	 */
	private Integer illegalpersonid;
	/**
	 * 
	 */
	private String post;
	/**
	 * 
	 */
	private String posttype;
	/**
	 * 
	 */
	private String employmentproperty;
	/**
	 * 
	 */
	private String workingyears;
	/**
	 * 
	 */
	private String education;
	/**
	 * 
	 */
	private String responsicompanyname;
	/**
	 * 
	 */
	private String responsicompanycode;
	/**
	 * 
	 */
	private Integer responsepersonid;
	/**
	 * 
	 */
	private String responsepersonname;
	/**
	 * 
	 */
	private Integer resverifierid;
	/**
	 * 
	 */
	private String resverifiername;
	/**
	 * 
	 */
	private String resverifydate;
	/**
	 * 
	 */
	private String resverifyadvice;
	/**
	 * 
	 */
	private Integer cheverifierid;
	/**
	 * 
	 */
	private String cheverifiername;
	/**
	 * 
	 */
	private String cheverifydate;
	/**
	 * 
	 */
	private String cheverifyadvice;
	/**
	 * 
	 */
	private Date reformdate;
	/**
	 * 
	 */
	private String reformlimit;
	/**
	 * 
	 */
	private String problemattach;
	/**
	 * 
	 */
	private String problempic;
	/**
	 * 
	 */
	private String correctattach;
	/**
	 * 
	 */
	private String correctpic;
	/**
	 * 
	 */
	private String ispush;

}
