package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:46
 */
@Data
@TableName("quality_checktablerecord")
public class QualityChecktablerecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer qulityChecktablerecordid;
	/**
	 * 
	 */
	private Integer qulityCheckid;
	/**
	 * 
	 */
	private String qualityCheckname;
	/**
	 * 
	 */
	private String checklistcode;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String checkresult;
	/**
	 * 
	 */
	private String attach;
	/**
	 * 
	 */
	private String pic;

}
