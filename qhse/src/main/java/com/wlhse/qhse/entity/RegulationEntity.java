package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@Data
@TableName("regulation")
public class RegulationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String typecode;
	/**
	 * 
	 */
	private String typename;
	/**
	 * 
	 */
	private String regname;
	/**
	 * 
	 */
	private String documentsymbol;
	/**
	 * 
	 */
	private String publishcomcode;
	/**
	 * 
	 */
	private String publishcomname;
	/**
	 * 
	 */
	private Date begindate;
	/**
	 * 
	 */
	private Date uploaddate;
	/**
	 * 
	 */
	private String regnamecode;

}
