package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:48
 */
@Data
@TableName("regulationrecord")
public class RegulationrecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Integer checkid;
	/**
	 * 
	 */
	private Integer safestaffId;
	/**
	 * 
	 */
	private String safestaffName;
	/**
	 * 
	 */
	private String workitem;
	/**
	 * 
	 */
	private String companyid;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private Date supervisiondate;
	/**
	 * 
	 */
	private String type;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String punish;
	/**
	 * 
	 */
	private Integer approve;
	/**
	 * 
	 */
	private Integer ok;
	/**
	 * 
	 */
	private Integer consequenceid;
	/**
	 * 
	 */
	private Integer checktype;
	/**
	 * 
	 */
	private String affix;
	/**
	 * 
	 */
	private String affixname;
	/**
	 * 
	 */
	private String checkname;
	/**
	 * 
	 */
	private Date updatetime;
	/**
	 * 
	 */
	private Date recorddate;
	/**
	 * 
	 */
	private Float score;
	/**
	 * 
	 */
	private Integer punish1;
	/**
	 * 
	 */
	private String employeecharacter;
	/**
	 * 
	 */
	private String workseniority;
	/**
	 * 
	 */
	private String position;
	/**
	 * 
	 */
	private String regulationcharacter;
	/**
	 * 
	 */
	private String factorsource;
	/**
	 * 
	 */
	private String professionid;
	/**
	 * 
	 */
	private String profession;
	/**
	 * 
	 */
	private String factorhse;
	/**
	 * 
	 */
	private String factordepartment;
	/**
	 * 
	 */
	private String factorobserver;
	/**
	 * 
	 */
	private String location;
	/**
	 * 
	 */
	private String affix1;
	/**
	 * 
	 */
	private String affix2;
	/**
	 * 
	 */
	private String affix3;
	/**
	 * 
	 */
	private String affix4;
	/**
	 * 
	 */
	private Integer isupload;
	/**
	 * 
	 */
	private String keyid;
	/**
	 * 
	 */
	private String qhseChecktype;
	/**
	 * 
	 */
	private String qhseCheckcategory;
	/**
	 * 
	 */
	private Integer qhseFileauditId;
	/**
	 * 
	 */
	private Integer qhseFileauditrecordId;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String regulationsource;
	/**
	 * 
	 */
	private Integer regulationid;
	/**
	 * 
	 */
	private String regulationname;
	/**
	 * 
	 */
	private String consequence;

}
