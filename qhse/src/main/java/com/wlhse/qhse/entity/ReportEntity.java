package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:54
 */
@Data
@TableName("report")
public class ReportEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer reportid;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String reportcode;
	/**
	 * 
	 */
	private String reporttype;
	/**
	 * 
	 */
	private Integer reportcheckpersonid;
	/**
	 * 
	 */
	private String reportcheckpersonname;
	/**
	 * 
	 */
	private Date reportplandate;
	/**
	 * 
	 */
	private String auditorid;
	/**
	 * 
	 */
	private String auditorname;
	/**
	 * 
	 */
	private String auditorcategory;
	/**
	 * 
	 */
	private Date auditordate;
	/**
	 * 
	 */
	private String approverid;
	/**
	 * 
	 */
	private String approvername;
	/**
	 * 
	 */
	private String approvercategory;
	/**
	 * 
	 */
	private Date approverdate;
	/**
	 * 
	 */
	private Date filedate;
	/**
	 * 
	 */
	private Integer senderid;
	/**
	 * 
	 */
	private String sendername;
	/**
	 * 
	 */
	private Date senddate;
	/**
	 * 
	 */
	private Integer reportcount;
	/**
	 * 
	 */
	private Integer seal1;
	/**
	 * 
	 */
	private Integer seal2;
	/**
	 * 
	 */
	private Integer seal3;
	/**
	 * 
	 */
	private Integer seal4;
	/**
	 * 
	 */
	private Integer seal5;
	/**
	 * 
	 */
	private Integer seal6;
	/**
	 * 
	 */
	private String note;
	/**
	 * 
	 */
	private Integer sealpersonid;
	/**
	 * 
	 */
	private String sealpersonname;
	/**
	 * 
	 */
	private Integer authid;
	/**
	 * 
	 */
	private String authname;
	/**
	 * 
	 */
	private Date sealdate;

}
