package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@Data
@TableName("report_sample")
public class ReportSampleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer sampleid;
	/**
	 * 
	 */
	private Integer reportid;
	/**
	 * 
	 */
	private String samplename;
	/**
	 * 
	 */
	private String sampleno;
	/**
	 * 
	 */
	private String samplemodel;
	/**
	 * 
	 */
	private String samplecode;
	/**
	 * 
	 */
	private String entrustcompany;
	/**
	 * 
	 */
	private String productcompany;
	/**
	 * 
	 */
	private String customercompany;
	/**
	 * 
	 */
	private Date arrivedate;
	/**
	 * 
	 */
	private Date checkdate;
	/**
	 * 
	 */
	private String checkaddress;
	/**
	 * 
	 */
	private String checkproject;
	/**
	 * 
	 */
	private String checkgist;
	/**
	 * 
	 */
	private String checkresult;
	/**
	 * 
	 */
	private String samplecheckpersonname;

}
