package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@Data
@TableName("report_unit_price")
public class ReportUnitPriceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer unitpriceid;
	/**
	 * 
	 */
	private String reporttype;
	/**
	 * 
	 */
	private Float unitprice;
	/**
	 * 
	 */
	private Float auditorproportion;
	/**
	 * 
	 */
	private Float approverproportion;
	/**
	 * 
	 */
	private String costyear;
	/**
	 * 
	 */
	private String hash;

}
