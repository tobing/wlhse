package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@Data
@TableName("safety_problem")
public class SafetyProblemEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer problemid;
	/**
	 * 
	 */
	private Integer checkstaffid;
	/**
	 * 
	 */
	private String checkstaffname;
	/**
	 * 
	 */
	private String problemcategory;
	/**
	 * 
	 */
	private String checkcategory;
	/**
	 * 
	 */
	private String checktype;
	/**
	 * 
	 */
	private String checkdate;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String companyname;
	/**
	 * 
	 */
	private String parentcompanyname;
	/**
	 * 
	 */
	private String address;
	/**
	 * 
	 */
	private String checkitemcode;
	/**
	 * 
	 */
	private String checkitemname;
	/**
	 * 
	 */
	private String checklistitemcode;
	/**
	 * 
	 */
	private String checklistitemname;
	/**
	 * 
	 */
	private String problemdescription;
	/**
	 * 
	 */
	private String responsepersonid;
	/**
	 * 
	 */
	private String responsepersonname;
	/**
	 * 
	 */
	private Integer employmentproperty;
	/**
	 * 
	 */
	private Float workingage;
	/**
	 * 
	 */
	private String workingname;
	/**
	 * 
	 */
	private String supervisorid;
	/**
	 * 
	 */
	private Integer supervisorname;
	/**
	 * 
	 */
	private String checkattach1;
	/**
	 * 
	 */
	private String checkattach2;
	/**
	 * 
	 */
	private String checkattach3;
	/**
	 * 
	 */
	private String checkattach4;
	/**
	 * 
	 */
	private String checkvideo;
	/**
	 * 
	 */
	private String immediately;
	/**
	 * 
	 */
	private Integer lostscore;
	/**
	 * 
	 */
	private Integer penalty;
	/**
	 * 
	 */
	private String education;
	/**
	 * 
	 */
	private String rectification;
	/**
	 * 
	 */
	private String rectattach1;
	/**
	 * 
	 */
	private String rectattach2;
	/**
	 * 
	 */
	private String rectattach3;
	/**
	 * 
	 */
	private String rectattach4;
	/**
	 * 
	 */
	private String rectvideo;
	/**
	 * 
	 */
	private Integer verifypersonempid;
	/**
	 * 
	 */
	private String verifypersonname;
	/**
	 * 
	 */
	private String verifydate;
	/**
	 * 
	 */
	private String recteffect;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private Integer checklistid;
	/**
	 * 
	 */
	private String checklistname;
	/**
	 * 
	 */
	private String checkrecordno;
	/**
	 * 
	 */
	private String recordstauts;
	/**
	 * 
	 */
	private String basiccompanyname;
	/**
	 * 
	 */
	private String note;
	/**
	 * 
	 */
	private String rectdescription;
	/**
	 * 
	 */
	private String rectfile;

}
