package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@Data
@TableName("sys_company")
public class SysCompanyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer sysCompanyid;
	/**
	 * 
	 */
	private String companycode;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private Integer serialno;
	/**
	 * 
	 */
	private String category1;
	/**
	 * 
	 */
	private String category2;
	/**
	 * 
	 */
	private String category3;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private String hash;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String upcpmpanyid;

}
