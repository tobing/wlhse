package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:53
 */
@Data
@TableName("sys_rolemodule")
public class SysRolemoduleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer sysRolemoduleId;
	/**
	 * 
	 */
	private String modulecode;
	/**
	 * 
	 */
	private String rolecode;

}
