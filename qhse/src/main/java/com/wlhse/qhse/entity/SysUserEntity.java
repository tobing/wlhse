package com.wlhse.qhse.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:52
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer sysUserId;
	/**
	 * 
	 */
	private Integer employeeid;
	/**
	 * 
	 */
	private String uname;
	/**
	 * 
	 */
	private String pwd;
	/**
	 * 
	 */
	private String status;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String openid;

}
