package com.wlhse.qhse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementEntity;

import java.util.Map;

/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:49
 */
public interface QhseCompanyyearmanagersyselementService extends IService<QhseCompanyyearmanagersyselementEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

