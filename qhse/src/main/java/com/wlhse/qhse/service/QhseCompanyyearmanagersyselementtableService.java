package com.wlhse.qhse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementtableEntity;

import java.util.Map;

/**
 * 
 *
 * @author tobing
 * @email tobing6379@gmail.com
 * @date 2021-03-07 16:13:51
 */
public interface QhseCompanyyearmanagersyselementtableService extends IService<QhseCompanyyearmanagersyselementtableEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

