package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ChecklistDao;
import com.wlhse.qhse.entity.ChecklistEntity;
import com.wlhse.qhse.service.ChecklistService;


@Service("checklistService")
public class ChecklistServiceImpl extends ServiceImpl<ChecklistDao, ChecklistEntity> implements ChecklistService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ChecklistEntity> page = this.page(
                new Query<ChecklistEntity>().getPage(params),
                new QueryWrapper<ChecklistEntity>()
        );

        return new PageUtils(page);
    }

}