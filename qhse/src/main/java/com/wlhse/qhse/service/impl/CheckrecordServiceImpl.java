package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.CheckrecordDao;
import com.wlhse.qhse.entity.CheckrecordEntity;
import com.wlhse.qhse.service.CheckrecordService;


@Service("checkrecordService")
public class CheckrecordServiceImpl extends ServiceImpl<CheckrecordDao, CheckrecordEntity> implements CheckrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CheckrecordEntity> page = this.page(
                new Query<CheckrecordEntity>().getPage(params),
                new QueryWrapper<CheckrecordEntity>()
        );

        return new PageUtils(page);
    }

}