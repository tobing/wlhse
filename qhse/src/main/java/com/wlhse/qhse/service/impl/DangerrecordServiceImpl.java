package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DangerrecordDao;
import com.wlhse.qhse.entity.DangerrecordEntity;
import com.wlhse.qhse.service.DangerrecordService;


@Service("dangerrecordService")
public class DangerrecordServiceImpl extends ServiceImpl<DangerrecordDao, DangerrecordEntity> implements DangerrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DangerrecordEntity> page = this.page(
                new Query<DangerrecordEntity>().getPage(params),
                new QueryWrapper<DangerrecordEntity>()
        );

        return new PageUtils(page);
    }

}