package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardEnvironmentManagementDao;
import com.wlhse.qhse.entity.DashboardEnvironmentManagementEntity;
import com.wlhse.qhse.service.DashboardEnvironmentManagementService;


@Service("dashboardEnvironmentManagementService")
public class DashboardEnvironmentManagementServiceImpl extends ServiceImpl<DashboardEnvironmentManagementDao, DashboardEnvironmentManagementEntity> implements DashboardEnvironmentManagementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardEnvironmentManagementEntity> page = this.page(
                new Query<DashboardEnvironmentManagementEntity>().getPage(params),
                new QueryWrapper<DashboardEnvironmentManagementEntity>()
        );

        return new PageUtils(page);
    }

}