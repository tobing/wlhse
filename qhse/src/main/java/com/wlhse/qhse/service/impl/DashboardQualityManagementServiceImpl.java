package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardQualityManagementDao;
import com.wlhse.qhse.entity.DashboardQualityManagementEntity;
import com.wlhse.qhse.service.DashboardQualityManagementService;


@Service("dashboardQualityManagementService")
public class DashboardQualityManagementServiceImpl extends ServiceImpl<DashboardQualityManagementDao, DashboardQualityManagementEntity> implements DashboardQualityManagementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardQualityManagementEntity> page = this.page(
                new Query<DashboardQualityManagementEntity>().getPage(params),
                new QueryWrapper<DashboardQualityManagementEntity>()
        );

        return new PageUtils(page);
    }

}