package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardRecorderManagementDao;
import com.wlhse.qhse.entity.DashboardRecorderManagementEntity;
import com.wlhse.qhse.service.DashboardRecorderManagementService;


@Service("dashboardRecorderManagementService")
public class DashboardRecorderManagementServiceImpl extends ServiceImpl<DashboardRecorderManagementDao, DashboardRecorderManagementEntity> implements DashboardRecorderManagementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardRecorderManagementEntity> page = this.page(
                new Query<DashboardRecorderManagementEntity>().getPage(params),
                new QueryWrapper<DashboardRecorderManagementEntity>()
        );

        return new PageUtils(page);
    }

}