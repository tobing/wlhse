package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardScheduleManagementDao;
import com.wlhse.qhse.entity.DashboardScheduleManagementEntity;
import com.wlhse.qhse.service.DashboardScheduleManagementService;


@Service("dashboardScheduleManagementService")
public class DashboardScheduleManagementServiceImpl extends ServiceImpl<DashboardScheduleManagementDao, DashboardScheduleManagementEntity> implements DashboardScheduleManagementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardScheduleManagementEntity> page = this.page(
                new Query<DashboardScheduleManagementEntity>().getPage(params),
                new QueryWrapper<DashboardScheduleManagementEntity>()
        );

        return new PageUtils(page);
    }

}