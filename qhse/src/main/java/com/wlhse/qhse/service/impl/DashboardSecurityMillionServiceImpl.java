package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardSecurityMillionDao;
import com.wlhse.qhse.entity.DashboardSecurityMillionEntity;
import com.wlhse.qhse.service.DashboardSecurityMillionService;


@Service("dashboardSecurityMillionService")
public class DashboardSecurityMillionServiceImpl extends ServiceImpl<DashboardSecurityMillionDao, DashboardSecurityMillionEntity> implements DashboardSecurityMillionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardSecurityMillionEntity> page = this.page(
                new Query<DashboardSecurityMillionEntity>().getPage(params),
                new QueryWrapper<DashboardSecurityMillionEntity>()
        );

        return new PageUtils(page);
    }

}