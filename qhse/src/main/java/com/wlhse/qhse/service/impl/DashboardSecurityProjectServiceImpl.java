package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardSecurityProjectDao;
import com.wlhse.qhse.entity.DashboardSecurityProjectEntity;
import com.wlhse.qhse.service.DashboardSecurityProjectService;


@Service("dashboardSecurityProjectService")
public class DashboardSecurityProjectServiceImpl extends ServiceImpl<DashboardSecurityProjectDao, DashboardSecurityProjectEntity> implements DashboardSecurityProjectService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardSecurityProjectEntity> page = this.page(
                new Query<DashboardSecurityProjectEntity>().getPage(params),
                new QueryWrapper<DashboardSecurityProjectEntity>()
        );

        return new PageUtils(page);
    }

}