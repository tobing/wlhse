package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DashboardSercurityManagementDao;
import com.wlhse.qhse.entity.DashboardSercurityManagementEntity;
import com.wlhse.qhse.service.DashboardSercurityManagementService;


@Service("dashboardSercurityManagementService")
public class DashboardSercurityManagementServiceImpl extends ServiceImpl<DashboardSercurityManagementDao, DashboardSercurityManagementEntity> implements DashboardSercurityManagementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DashboardSercurityManagementEntity> page = this.page(
                new Query<DashboardSercurityManagementEntity>().getPage(params),
                new QueryWrapper<DashboardSercurityManagementEntity>()
        );

        return new PageUtils(page);
    }

}