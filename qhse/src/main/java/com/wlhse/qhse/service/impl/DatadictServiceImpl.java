package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.DatadictDao;
import com.wlhse.qhse.entity.DatadictEntity;
import com.wlhse.qhse.service.DatadictService;


@Service("datadictService")
public class DatadictServiceImpl extends ServiceImpl<DatadictDao, DatadictEntity> implements DatadictService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DatadictEntity> page = this.page(
                new Query<DatadictEntity>().getPage(params),
                new QueryWrapper<DatadictEntity>()
        );

        return new PageUtils(page);
    }

}