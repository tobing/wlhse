package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ElementinputfileinfoDao;
import com.wlhse.qhse.entity.ElementinputfileinfoEntity;
import com.wlhse.qhse.service.ElementinputfileinfoService;


@Service("elementinputfileinfoService")
public class ElementinputfileinfoServiceImpl extends ServiceImpl<ElementinputfileinfoDao, ElementinputfileinfoEntity> implements ElementinputfileinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ElementinputfileinfoEntity> page = this.page(
                new Query<ElementinputfileinfoEntity>().getPage(params),
                new QueryWrapper<ElementinputfileinfoEntity>()
        );

        return new PageUtils(page);
    }

}