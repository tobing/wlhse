package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FactorDao;
import com.wlhse.qhse.entity.FactorEntity;
import com.wlhse.qhse.service.FactorService;


@Service("factorService")
public class FactorServiceImpl extends ServiceImpl<FactorDao, FactorEntity> implements FactorService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FactorEntity> page = this.page(
                new Query<FactorEntity>().getPage(params),
                new QueryWrapper<FactorEntity>()
        );

        return new PageUtils(page);
    }

}