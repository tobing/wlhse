package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FactordepartmentDao;
import com.wlhse.qhse.entity.FactordepartmentEntity;
import com.wlhse.qhse.service.FactordepartmentService;


@Service("factordepartmentService")
public class FactordepartmentServiceImpl extends ServiceImpl<FactordepartmentDao, FactordepartmentEntity> implements FactordepartmentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FactordepartmentEntity> page = this.page(
                new Query<FactordepartmentEntity>().getPage(params),
                new QueryWrapper<FactordepartmentEntity>()
        );

        return new PageUtils(page);
    }

}