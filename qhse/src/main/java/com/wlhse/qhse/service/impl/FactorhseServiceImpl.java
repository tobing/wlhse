package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FactorhseDao;
import com.wlhse.qhse.entity.FactorhseEntity;
import com.wlhse.qhse.service.FactorhseService;


@Service("factorhseService")
public class FactorhseServiceImpl extends ServiceImpl<FactorhseDao, FactorhseEntity> implements FactorhseService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FactorhseEntity> page = this.page(
                new Query<FactorhseEntity>().getPage(params),
                new QueryWrapper<FactorhseEntity>()
        );

        return new PageUtils(page);
    }

}