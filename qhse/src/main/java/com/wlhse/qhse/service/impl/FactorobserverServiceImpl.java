package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FactorobserverDao;
import com.wlhse.qhse.entity.FactorobserverEntity;
import com.wlhse.qhse.service.FactorobserverService;


@Service("factorobserverService")
public class FactorobserverServiceImpl extends ServiceImpl<FactorobserverDao, FactorobserverEntity> implements FactorobserverService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FactorobserverEntity> page = this.page(
                new Query<FactorobserverEntity>().getPage(params),
                new QueryWrapper<FactorobserverEntity>()
        );

        return new PageUtils(page);
    }

}