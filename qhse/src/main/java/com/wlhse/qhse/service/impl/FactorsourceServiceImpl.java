package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FactorsourceDao;
import com.wlhse.qhse.entity.FactorsourceEntity;
import com.wlhse.qhse.service.FactorsourceService;


@Service("factorsourceService")
public class FactorsourceServiceImpl extends ServiceImpl<FactorsourceDao, FactorsourceEntity> implements FactorsourceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FactorsourceEntity> page = this.page(
                new Query<FactorsourceEntity>().getPage(params),
                new QueryWrapper<FactorsourceEntity>()
        );

        return new PageUtils(page);
    }

}