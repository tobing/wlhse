package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FilePropagationfileinfoDao;
import com.wlhse.qhse.entity.FilePropagationfileinfoEntity;
import com.wlhse.qhse.service.FilePropagationfileinfoService;


@Service("filePropagationfileinfoService")
public class FilePropagationfileinfoServiceImpl extends ServiceImpl<FilePropagationfileinfoDao, FilePropagationfileinfoEntity> implements FilePropagationfileinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FilePropagationfileinfoEntity> page = this.page(
                new Query<FilePropagationfileinfoEntity>().getPage(params),
                new QueryWrapper<FilePropagationfileinfoEntity>()
        );

        return new PageUtils(page);
    }

}