package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FilePropagationplanDao;
import com.wlhse.qhse.entity.FilePropagationplanEntity;
import com.wlhse.qhse.service.FilePropagationplanService;


@Service("filePropagationplanService")
public class FilePropagationplanServiceImpl extends ServiceImpl<FilePropagationplanDao, FilePropagationplanEntity> implements FilePropagationplanService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FilePropagationplanEntity> page = this.page(
                new Query<FilePropagationplanEntity>().getPage(params),
                new QueryWrapper<FilePropagationplanEntity>()
        );

        return new PageUtils(page);
    }

}