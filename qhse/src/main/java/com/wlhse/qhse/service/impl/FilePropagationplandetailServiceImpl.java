package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.FilePropagationplandetailDao;
import com.wlhse.qhse.entity.FilePropagationplandetailEntity;
import com.wlhse.qhse.service.FilePropagationplandetailService;


@Service("filePropagationplandetailService")
public class FilePropagationplandetailServiceImpl extends ServiceImpl<FilePropagationplandetailDao, FilePropagationplandetailEntity> implements FilePropagationplandetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FilePropagationplandetailEntity> page = this.page(
                new Query<FilePropagationplandetailEntity>().getPage(params),
                new QueryWrapper<FilePropagationplandetailEntity>()
        );

        return new PageUtils(page);
    }

}