package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.InterfaceModuleDao;
import com.wlhse.qhse.entity.InterfaceModuleEntity;
import com.wlhse.qhse.service.InterfaceModuleService;


@Service("interfaceModuleService")
public class InterfaceModuleServiceImpl extends ServiceImpl<InterfaceModuleDao, InterfaceModuleEntity> implements InterfaceModuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InterfaceModuleEntity> page = this.page(
                new Query<InterfaceModuleEntity>().getPage(params),
                new QueryWrapper<InterfaceModuleEntity>()
        );

        return new PageUtils(page);
    }

}