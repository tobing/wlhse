package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MeschecksumdataDao;
import com.wlhse.qhse.entity.MeschecksumdataEntity;
import com.wlhse.qhse.service.MeschecksumdataService;


@Service("meschecksumdataService")
public class MeschecksumdataServiceImpl extends ServiceImpl<MeschecksumdataDao, MeschecksumdataEntity> implements MeschecksumdataService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MeschecksumdataEntity> page = this.page(
                new Query<MeschecksumdataEntity>().getPage(params),
                new QueryWrapper<MeschecksumdataEntity>()
        );

        return new PageUtils(page);
    }

}