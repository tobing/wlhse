package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MessumdateDao;
import com.wlhse.qhse.entity.MessumdateEntity;
import com.wlhse.qhse.service.MessumdateService;


@Service("messumdateService")
public class MessumdateServiceImpl extends ServiceImpl<MessumdateDao, MessumdateEntity> implements MessumdateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MessumdateEntity> page = this.page(
                new Query<MessumdateEntity>().getPage(params),
                new QueryWrapper<MessumdateEntity>()
        );

        return new PageUtils(page);
    }

}