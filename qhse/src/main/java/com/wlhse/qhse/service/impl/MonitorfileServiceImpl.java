package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MonitorfileDao;
import com.wlhse.qhse.entity.MonitorfileEntity;
import com.wlhse.qhse.service.MonitorfileService;


@Service("monitorfileService")
public class MonitorfileServiceImpl extends ServiceImpl<MonitorfileDao, MonitorfileEntity> implements MonitorfileService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MonitorfileEntity> page = this.page(
                new Query<MonitorfileEntity>().getPage(params),
                new QueryWrapper<MonitorfileEntity>()
        );

        return new PageUtils(page);
    }

}