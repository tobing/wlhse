package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MonitorinputcheckrecordDao;
import com.wlhse.qhse.entity.MonitorinputcheckrecordEntity;
import com.wlhse.qhse.service.MonitorinputcheckrecordService;


@Service("monitorinputcheckrecordService")
public class MonitorinputcheckrecordServiceImpl extends ServiceImpl<MonitorinputcheckrecordDao, MonitorinputcheckrecordEntity> implements MonitorinputcheckrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MonitorinputcheckrecordEntity> page = this.page(
                new Query<MonitorinputcheckrecordEntity>().getPage(params),
                new QueryWrapper<MonitorinputcheckrecordEntity>()
        );

        return new PageUtils(page);
    }

}