package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MonitorplanDao;
import com.wlhse.qhse.entity.MonitorplanEntity;
import com.wlhse.qhse.service.MonitorplanService;


@Service("monitorplanService")
public class MonitorplanServiceImpl extends ServiceImpl<MonitorplanDao, MonitorplanEntity> implements MonitorplanService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MonitorplanEntity> page = this.page(
                new Query<MonitorplanEntity>().getPage(params),
                new QueryWrapper<MonitorplanEntity>()
        );

        return new PageUtils(page);
    }

}