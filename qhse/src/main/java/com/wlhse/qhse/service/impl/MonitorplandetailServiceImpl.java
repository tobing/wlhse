package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.MonitorplandetailDao;
import com.wlhse.qhse.entity.MonitorplandetailEntity;
import com.wlhse.qhse.service.MonitorplandetailService;


@Service("monitorplandetailService")
public class MonitorplandetailServiceImpl extends ServiceImpl<MonitorplandetailDao, MonitorplandetailEntity> implements MonitorplandetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MonitorplandetailEntity> page = this.page(
                new Query<MonitorplandetailEntity>().getPage(params),
                new QueryWrapper<MonitorplandetailEntity>()
        );

        return new PageUtils(page);
    }

}