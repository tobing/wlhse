package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseAccidentDao;
import com.wlhse.qhse.entity.QhseAccidentEntity;
import com.wlhse.qhse.service.QhseAccidentService;


@Service("qhseAccidentService")
public class QhseAccidentServiceImpl extends ServiceImpl<QhseAccidentDao, QhseAccidentEntity> implements QhseAccidentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseAccidentEntity> page = this.page(
                new Query<QhseAccidentEntity>().getPage(params),
                new QueryWrapper<QhseAccidentEntity>()
        );

        return new PageUtils(page);
    }

}