package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseAuditproblemrecordDao;
import com.wlhse.qhse.entity.QhseAuditproblemrecordEntity;
import com.wlhse.qhse.service.QhseAuditproblemrecordService;


@Service("qhseAuditproblemrecordService")
public class QhseAuditproblemrecordServiceImpl extends ServiceImpl<QhseAuditproblemrecordDao, QhseAuditproblemrecordEntity> implements QhseAuditproblemrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseAuditproblemrecordEntity> page = this.page(
                new Query<QhseAuditproblemrecordEntity>().getPage(params),
                new QueryWrapper<QhseAuditproblemrecordEntity>()
        );

        return new PageUtils(page);
    }

}