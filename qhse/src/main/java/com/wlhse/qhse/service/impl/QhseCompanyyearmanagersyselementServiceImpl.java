package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseCompanyyearmanagersyselementDao;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementService;


@Service("qhseCompanyyearmanagersyselementService")
public class QhseCompanyyearmanagersyselementServiceImpl extends ServiceImpl<QhseCompanyyearmanagersyselementDao, QhseCompanyyearmanagersyselementEntity> implements QhseCompanyyearmanagersyselementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseCompanyyearmanagersyselementEntity> page = this.page(
                new Query<QhseCompanyyearmanagersyselementEntity>().getPage(params),
                new QueryWrapper<QhseCompanyyearmanagersyselementEntity>()
        );

        return new PageUtils(page);
    }

}