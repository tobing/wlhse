package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseCompanyyearmanagersyselementevidenceDao;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementevidenceEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementevidenceService;


@Service("qhseCompanyyearmanagersyselementevidenceService")
public class QhseCompanyyearmanagersyselementevidenceServiceImpl extends ServiceImpl<QhseCompanyyearmanagersyselementevidenceDao, QhseCompanyyearmanagersyselementevidenceEntity> implements QhseCompanyyearmanagersyselementevidenceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseCompanyyearmanagersyselementevidenceEntity> page = this.page(
                new Query<QhseCompanyyearmanagersyselementevidenceEntity>().getPage(params),
                new QueryWrapper<QhseCompanyyearmanagersyselementevidenceEntity>()
        );

        return new PageUtils(page);
    }

}