package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseCompanyyearmanagersyselementevidenceattachDao;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementevidenceattachEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementevidenceattachService;


@Service("qhseCompanyyearmanagersyselementevidenceattachService")
public class QhseCompanyyearmanagersyselementevidenceattachServiceImpl extends ServiceImpl<QhseCompanyyearmanagersyselementevidenceattachDao, QhseCompanyyearmanagersyselementevidenceattachEntity> implements QhseCompanyyearmanagersyselementevidenceattachService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseCompanyyearmanagersyselementevidenceattachEntity> page = this.page(
                new Query<QhseCompanyyearmanagersyselementevidenceattachEntity>().getPage(params),
                new QueryWrapper<QhseCompanyyearmanagersyselementevidenceattachEntity>()
        );

        return new PageUtils(page);
    }

}