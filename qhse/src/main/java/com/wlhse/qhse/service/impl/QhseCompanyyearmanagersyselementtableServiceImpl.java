package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseCompanyyearmanagersyselementtableDao;
import com.wlhse.qhse.entity.QhseCompanyyearmanagersyselementtableEntity;
import com.wlhse.qhse.service.QhseCompanyyearmanagersyselementtableService;


@Service("qhseCompanyyearmanagersyselementtableService")
public class QhseCompanyyearmanagersyselementtableServiceImpl extends ServiceImpl<QhseCompanyyearmanagersyselementtableDao, QhseCompanyyearmanagersyselementtableEntity> implements QhseCompanyyearmanagersyselementtableService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseCompanyyearmanagersyselementtableEntity> page = this.page(
                new Query<QhseCompanyyearmanagersyselementtableEntity>().getPage(params),
                new QueryWrapper<QhseCompanyyearmanagersyselementtableEntity>()
        );

        return new PageUtils(page);
    }

}