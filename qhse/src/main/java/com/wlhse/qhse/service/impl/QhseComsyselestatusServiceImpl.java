package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseComsyselestatusDao;
import com.wlhse.qhse.entity.QhseComsyselestatusEntity;
import com.wlhse.qhse.service.QhseComsyselestatusService;


@Service("qhseComsyselestatusService")
public class QhseComsyselestatusServiceImpl extends ServiceImpl<QhseComsyselestatusDao, QhseComsyselestatusEntity> implements QhseComsyselestatusService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseComsyselestatusEntity> page = this.page(
                new Query<QhseComsyselestatusEntity>().getPage(params),
                new QueryWrapper<QhseComsyselestatusEntity>()
        );

        return new PageUtils(page);
    }

}