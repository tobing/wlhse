package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseEventDao;
import com.wlhse.qhse.entity.QhseEventEntity;
import com.wlhse.qhse.service.QhseEventService;


@Service("qhseEventService")
public class QhseEventServiceImpl extends ServiceImpl<QhseEventDao, QhseEventEntity> implements QhseEventService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseEventEntity> page = this.page(
                new Query<QhseEventEntity>().getPage(params),
                new QueryWrapper<QhseEventEntity>()
        );

        return new PageUtils(page);
    }

}