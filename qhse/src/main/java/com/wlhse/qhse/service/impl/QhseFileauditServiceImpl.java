package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseFileauditDao;
import com.wlhse.qhse.entity.QhseFileauditEntity;
import com.wlhse.qhse.service.QhseFileauditService;


@Service("qhseFileauditService")
public class QhseFileauditServiceImpl extends ServiceImpl<QhseFileauditDao, QhseFileauditEntity> implements QhseFileauditService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseFileauditEntity> page = this.page(
                new Query<QhseFileauditEntity>().getPage(params),
                new QueryWrapper<QhseFileauditEntity>()
        );

        return new PageUtils(page);
    }

}