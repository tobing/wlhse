package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseFileauditrecordDao;
import com.wlhse.qhse.entity.QhseFileauditrecordEntity;
import com.wlhse.qhse.service.QhseFileauditrecordService;


@Service("qhseFileauditrecordService")
public class QhseFileauditrecordServiceImpl extends ServiceImpl<QhseFileauditrecordDao, QhseFileauditrecordEntity> implements QhseFileauditrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseFileauditrecordEntity> page = this.page(
                new Query<QhseFileauditrecordEntity>().getPage(params),
                new QueryWrapper<QhseFileauditrecordEntity>()
        );

        return new PageUtils(page);
    }

}