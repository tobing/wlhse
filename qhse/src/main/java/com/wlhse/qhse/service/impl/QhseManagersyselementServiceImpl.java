package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseManagersyselementDao;
import com.wlhse.qhse.entity.QhseManagersyselementEntity;
import com.wlhse.qhse.service.QhseManagersyselementService;


@Service("qhseManagersyselementService")
public class QhseManagersyselementServiceImpl extends ServiceImpl<QhseManagersyselementDao, QhseManagersyselementEntity> implements QhseManagersyselementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseManagersyselementEntity> page = this.page(
                new Query<QhseManagersyselementEntity>().getPage(params),
                new QueryWrapper<QhseManagersyselementEntity>()
        );

        return new PageUtils(page);
    }

}