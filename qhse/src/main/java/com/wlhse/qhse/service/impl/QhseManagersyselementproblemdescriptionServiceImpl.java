package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseManagersyselementproblemdescriptionDao;
import com.wlhse.qhse.entity.QhseManagersyselementproblemdescriptionEntity;
import com.wlhse.qhse.service.QhseManagersyselementproblemdescriptionService;


@Service("qhseManagersyselementproblemdescriptionService")
public class QhseManagersyselementproblemdescriptionServiceImpl extends ServiceImpl<QhseManagersyselementproblemdescriptionDao, QhseManagersyselementproblemdescriptionEntity> implements QhseManagersyselementproblemdescriptionService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseManagersyselementproblemdescriptionEntity> page = this.page(
                new Query<QhseManagersyselementproblemdescriptionEntity>().getPage(params),
                new QueryWrapper<QhseManagersyselementproblemdescriptionEntity>()
        );

        return new PageUtils(page);
    }

}