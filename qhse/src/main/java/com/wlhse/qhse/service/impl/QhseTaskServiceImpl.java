package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QhseTaskDao;
import com.wlhse.qhse.entity.QhseTaskEntity;
import com.wlhse.qhse.service.QhseTaskService;


@Service("qhseTaskService")
public class QhseTaskServiceImpl extends ServiceImpl<QhseTaskDao, QhseTaskEntity> implements QhseTaskService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QhseTaskEntity> page = this.page(
                new Query<QhseTaskEntity>().getPage(params),
                new QueryWrapper<QhseTaskEntity>()
        );

        return new PageUtils(page);
    }

}