package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QualityCheckDao;
import com.wlhse.qhse.entity.QualityCheckEntity;
import com.wlhse.qhse.service.QualityCheckService;


@Service("qualityCheckService")
public class QualityCheckServiceImpl extends ServiceImpl<QualityCheckDao, QualityCheckEntity> implements QualityCheckService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QualityCheckEntity> page = this.page(
                new Query<QualityCheckEntity>().getPage(params),
                new QueryWrapper<QualityCheckEntity>()
        );

        return new PageUtils(page);
    }

}