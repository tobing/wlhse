package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QualityChecklistDao;
import com.wlhse.qhse.entity.QualityChecklistEntity;
import com.wlhse.qhse.service.QualityChecklistService;


@Service("qualityChecklistService")
public class QualityChecklistServiceImpl extends ServiceImpl<QualityChecklistDao, QualityChecklistEntity> implements QualityChecklistService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QualityChecklistEntity> page = this.page(
                new Query<QualityChecklistEntity>().getPage(params),
                new QueryWrapper<QualityChecklistEntity>()
        );

        return new PageUtils(page);
    }

}