package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QualityCheckrecordDao;
import com.wlhse.qhse.entity.QualityCheckrecordEntity;
import com.wlhse.qhse.service.QualityCheckrecordService;


@Service("qualityCheckrecordService")
public class QualityCheckrecordServiceImpl extends ServiceImpl<QualityCheckrecordDao, QualityCheckrecordEntity> implements QualityCheckrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QualityCheckrecordEntity> page = this.page(
                new Query<QualityCheckrecordEntity>().getPage(params),
                new QueryWrapper<QualityCheckrecordEntity>()
        );

        return new PageUtils(page);
    }

}