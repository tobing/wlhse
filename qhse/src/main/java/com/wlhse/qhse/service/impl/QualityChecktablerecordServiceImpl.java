package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QualityChecktablerecordDao;
import com.wlhse.qhse.entity.QualityChecktablerecordEntity;
import com.wlhse.qhse.service.QualityChecktablerecordService;


@Service("qualityChecktablerecordService")
public class QualityChecktablerecordServiceImpl extends ServiceImpl<QualityChecktablerecordDao, QualityChecktablerecordEntity> implements QualityChecktablerecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QualityChecktablerecordEntity> page = this.page(
                new Query<QualityChecktablerecordEntity>().getPage(params),
                new QueryWrapper<QualityChecktablerecordEntity>()
        );

        return new PageUtils(page);
    }

}