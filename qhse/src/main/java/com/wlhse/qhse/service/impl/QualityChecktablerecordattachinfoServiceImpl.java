package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.QualityChecktablerecordattachinfoDao;
import com.wlhse.qhse.entity.QualityChecktablerecordattachinfoEntity;
import com.wlhse.qhse.service.QualityChecktablerecordattachinfoService;


@Service("qualityChecktablerecordattachinfoService")
public class QualityChecktablerecordattachinfoServiceImpl extends ServiceImpl<QualityChecktablerecordattachinfoDao, QualityChecktablerecordattachinfoEntity> implements QualityChecktablerecordattachinfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<QualityChecktablerecordattachinfoEntity> page = this.page(
                new Query<QualityChecktablerecordattachinfoEntity>().getPage(params),
                new QueryWrapper<QualityChecktablerecordattachinfoEntity>()
        );

        return new PageUtils(page);
    }

}