package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.RegulationDao;
import com.wlhse.qhse.entity.RegulationEntity;
import com.wlhse.qhse.service.RegulationService;


@Service("regulationService")
public class RegulationServiceImpl extends ServiceImpl<RegulationDao, RegulationEntity> implements RegulationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RegulationEntity> page = this.page(
                new Query<RegulationEntity>().getPage(params),
                new QueryWrapper<RegulationEntity>()
        );

        return new PageUtils(page);
    }

}