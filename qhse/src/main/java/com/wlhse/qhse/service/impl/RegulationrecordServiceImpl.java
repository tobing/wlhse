package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.RegulationrecordDao;
import com.wlhse.qhse.entity.RegulationrecordEntity;
import com.wlhse.qhse.service.RegulationrecordService;


@Service("regulationrecordService")
public class RegulationrecordServiceImpl extends ServiceImpl<RegulationrecordDao, RegulationrecordEntity> implements RegulationrecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RegulationrecordEntity> page = this.page(
                new Query<RegulationrecordEntity>().getPage(params),
                new QueryWrapper<RegulationrecordEntity>()
        );

        return new PageUtils(page);
    }

}