package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportCodeRuleDao;
import com.wlhse.qhse.entity.ReportCodeRuleEntity;
import com.wlhse.qhse.service.ReportCodeRuleService;


@Service("reportCodeRuleService")
public class ReportCodeRuleServiceImpl extends ServiceImpl<ReportCodeRuleDao, ReportCodeRuleEntity> implements ReportCodeRuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportCodeRuleEntity> page = this.page(
                new Query<ReportCodeRuleEntity>().getPage(params),
                new QueryWrapper<ReportCodeRuleEntity>()
        );

        return new PageUtils(page);
    }

}