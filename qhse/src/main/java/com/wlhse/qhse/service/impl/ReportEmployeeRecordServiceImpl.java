package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportEmployeeRecordDao;
import com.wlhse.qhse.entity.ReportEmployeeRecordEntity;
import com.wlhse.qhse.service.ReportEmployeeRecordService;


@Service("reportEmployeeRecordService")
public class ReportEmployeeRecordServiceImpl extends ServiceImpl<ReportEmployeeRecordDao, ReportEmployeeRecordEntity> implements ReportEmployeeRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportEmployeeRecordEntity> page = this.page(
                new Query<ReportEmployeeRecordEntity>().getPage(params),
                new QueryWrapper<ReportEmployeeRecordEntity>()
        );

        return new PageUtils(page);
    }

}