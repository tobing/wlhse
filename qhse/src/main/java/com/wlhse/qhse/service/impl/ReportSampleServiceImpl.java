package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportSampleDao;
import com.wlhse.qhse.entity.ReportSampleEntity;
import com.wlhse.qhse.service.ReportSampleService;


@Service("reportSampleService")
public class ReportSampleServiceImpl extends ServiceImpl<ReportSampleDao, ReportSampleEntity> implements ReportSampleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportSampleEntity> page = this.page(
                new Query<ReportSampleEntity>().getPage(params),
                new QueryWrapper<ReportSampleEntity>()
        );

        return new PageUtils(page);
    }

}