package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportStandardDao;
import com.wlhse.qhse.entity.ReportStandardEntity;
import com.wlhse.qhse.service.ReportStandardService;


@Service("reportStandardService")
public class ReportStandardServiceImpl extends ServiceImpl<ReportStandardDao, ReportStandardEntity> implements ReportStandardService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportStandardEntity> page = this.page(
                new Query<ReportStandardEntity>().getPage(params),
                new QueryWrapper<ReportStandardEntity>()
        );

        return new PageUtils(page);
    }

}