package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportTotalCostDao;
import com.wlhse.qhse.entity.ReportTotalCostEntity;
import com.wlhse.qhse.service.ReportTotalCostService;


@Service("reportTotalCostService")
public class ReportTotalCostServiceImpl extends ServiceImpl<ReportTotalCostDao, ReportTotalCostEntity> implements ReportTotalCostService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportTotalCostEntity> page = this.page(
                new Query<ReportTotalCostEntity>().getPage(params),
                new QueryWrapper<ReportTotalCostEntity>()
        );

        return new PageUtils(page);
    }

}