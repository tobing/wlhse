package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.ReportUnitPriceDao;
import com.wlhse.qhse.entity.ReportUnitPriceEntity;
import com.wlhse.qhse.service.ReportUnitPriceService;


@Service("reportUnitPriceService")
public class ReportUnitPriceServiceImpl extends ServiceImpl<ReportUnitPriceDao, ReportUnitPriceEntity> implements ReportUnitPriceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReportUnitPriceEntity> page = this.page(
                new Query<ReportUnitPriceEntity>().getPage(params),
                new QueryWrapper<ReportUnitPriceEntity>()
        );

        return new PageUtils(page);
    }

}