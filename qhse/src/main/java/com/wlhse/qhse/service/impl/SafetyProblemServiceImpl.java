package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.SafetyProblemDao;
import com.wlhse.qhse.entity.SafetyProblemEntity;
import com.wlhse.qhse.service.SafetyProblemService;


@Service("safetyProblemService")
public class SafetyProblemServiceImpl extends ServiceImpl<SafetyProblemDao, SafetyProblemEntity> implements SafetyProblemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SafetyProblemEntity> page = this.page(
                new Query<SafetyProblemEntity>().getPage(params),
                new QueryWrapper<SafetyProblemEntity>()
        );

        return new PageUtils(page);
    }

}