package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.SysCompanyDao;
import com.wlhse.qhse.entity.SysCompanyEntity;
import com.wlhse.qhse.service.SysCompanyService;


@Service("sysCompanyService")
public class SysCompanyServiceImpl extends ServiceImpl<SysCompanyDao, SysCompanyEntity> implements SysCompanyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysCompanyEntity> page = this.page(
                new Query<SysCompanyEntity>().getPage(params),
                new QueryWrapper<SysCompanyEntity>()
        );

        return new PageUtils(page);
    }

}