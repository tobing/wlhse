package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.SysModuleDao;
import com.wlhse.qhse.entity.SysModuleEntity;
import com.wlhse.qhse.service.SysModuleService;


@Service("sysModuleService")
public class SysModuleServiceImpl extends ServiceImpl<SysModuleDao, SysModuleEntity> implements SysModuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysModuleEntity> page = this.page(
                new Query<SysModuleEntity>().getPage(params),
                new QueryWrapper<SysModuleEntity>()
        );

        return new PageUtils(page);
    }

}