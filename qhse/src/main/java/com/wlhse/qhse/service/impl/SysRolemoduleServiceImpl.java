package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.SysRolemoduleDao;
import com.wlhse.qhse.entity.SysRolemoduleEntity;
import com.wlhse.qhse.service.SysRolemoduleService;


@Service("sysRolemoduleService")
public class SysRolemoduleServiceImpl extends ServiceImpl<SysRolemoduleDao, SysRolemoduleEntity> implements SysRolemoduleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysRolemoduleEntity> page = this.page(
                new Query<SysRolemoduleEntity>().getPage(params),
                new QueryWrapper<SysRolemoduleEntity>()
        );

        return new PageUtils(page);
    }

}