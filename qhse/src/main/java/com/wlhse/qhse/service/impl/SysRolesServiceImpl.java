package com.wlhse.qhse.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlhse.common.utils.PageUtils;
import com.wlhse.common.utils.Query;

import com.wlhse.qhse.dao.SysRolesDao;
import com.wlhse.qhse.entity.SysRolesEntity;
import com.wlhse.qhse.service.SysRolesService;


@Service("sysRolesService")
public class SysRolesServiceImpl extends ServiceImpl<SysRolesDao, SysRolesEntity> implements SysRolesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysRolesEntity> page = this.page(
                new Query<SysRolesEntity>().getPage(params),
                new QueryWrapper<SysRolesEntity>()
        );

        return new PageUtils(page);
    }

}