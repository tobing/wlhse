package com.wlhse.common.utils;

/**
 * @Author tobing
 * @Date 2021/1/27 10:21
 * @Description
 */
public enum PlatformCode {
    OK(200, "正常访问"),
    NOT_FOUND(404, "资源找不到"),
    SERVER_ERROR(500, "服务器异常"),
    UNKNOWN_ERROR(600, "未知异常");

    private final Integer code;
    private final String msg;

    PlatformCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "PlatformCode{" +
                "msg='" + msg + '\'' +
                ", code=" + code +
                '}';
    }
}
